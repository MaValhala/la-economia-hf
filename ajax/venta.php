<?php 
require_once "../modelos/Venta.php";
if (strlen(session_id())<1) 
	session_start();

$venta = new Venta();

$idventa=isset($_POST["idventa"])? limpiarCadena($_POST["idventa"]):"";
$idcliente=isset($_POST["idcliente"])? limpiarCadena($_POST["idcliente"]):"";
$idusuario=$_SESSION["idusuario"];
$tipo_comprobante=isset($_POST["tipo_comprobante"])? limpiarCadena($_POST["tipo_comprobante"]):"";
$serie_comprobante=isset($_POST["serie_comprobante"])? limpiarCadena($_POST["serie_comprobante"]):"";
//$num_comprobante=isset($_POST["num_comprobante"])? limpiarCadena($_POST["num_comprobante"]):"";
$fecha_hora=isset($_POST["fecha_hora"])? limpiarCadena($_POST["fecha_hora"]):"";
$impuesto=isset($_POST["impuesto"])? limpiarCadena($_POST["impuesto"]):"";
$total_venta=isset($_POST["total_venta"])? limpiarCadena($_POST["total_venta"]):"";
$tipo_pago =isset($_POST["tipo_pago"])? limpiarCadena($_POST["tipo_pago"]):"";
$idvehiculo=isset($_POST["placa"])? limpiarCadena($_POST["placa"]):"";
// $idvehiculo=isset($_POST["placa"])? limpiarCadena($_POST["placa"]):"";
$observaciones =isset($_POST["observaciones"])? limpiarCadena($_POST["observaciones"]):"";





switch ($_GET["op"]) {
	case 'guardaryeditar':
	$id=$_GET['id'];
	if (empty($id)) {
		if($tipo_pago=='Credito'){
			$abono =isset($_POST["abono"])? limpiarCadena($_POST["abono"]):"";
		}else{
			$abono = $total_venta;
		}
		$rspta=$venta->insertar($idcliente,$idusuario,$tipo_comprobante,$serie_comprobante,$fecha_hora,$impuesto,$total_venta,$abono,$idvehiculo,$observaciones,$tipo_pago,$_POST["idarticulo"],$_POST["cantidad"],$_POST["precio_venta"],$_POST["descuento"]); 
		// para imprimir de wuan
		//echo $rspta == false? 'No se completo la venta' : $rspta;
		// para mostrar una alerta de venta completada
		echo $rspta == false? 'No se completo la venta' : 'Venta completada correctamente c:';
		
	}else{
		$abono =isset($_POST["abono"])? limpiarCadena($_POST["abono"]):"";
		if( array_key_exists("idarticulodev",$_POST)){
			$rspta = $venta->actualizar($id,$idusuario,$tipo_comprobante,$serie_comprobante,$impuesto,$abono,$idvehiculo,$observaciones,$total_venta,$_POST["idarticulodev"],$_POST["cantidaddev"],$_POST['iddev'],$_POST['reingresar']);
		
		}else{
			$rspta = $venta->actualizar($id,$idusuario,$tipo_comprobante,$serie_comprobante,$impuesto,$abono,$idvehiculo,$observaciones,$total_venta,[],[],[],[]);
		
		}
        echo $rspta ==false? 'Opps, hubo un error en el registro' : 'Registro completado exitosamente';
	}
		break;
	

	case 'anular':
		$rspta=$venta->anular($idventa);
		// to check sql code
		// echo $rspta;
		// to normal 
		echo $rspta == true ? "Ingreso anulado correctamente" :$rspta;
		break;
	case 'registrarPlaca':
		$idcliente = isset($_POST["idcliente"])? limpiarCadena($_POST["idcliente"]):"";
		$placa     = isset($_POST["placa"])? limpiarCadena($_POST["placa"]):"";
		$rspta=$venta->agregarPlaca($idcliente,$placa);
		echo $rspta ? "Placa registrada correctamente" : "Error al registrar la placa";
		break;
	
	case 'mostrar':
		if($_SESSION['nombre']=='SuperAdmin' OR $_SESSION['facturas']==1){
			$rspta=$venta->mostrar($idventa);
			$temp = json_encode($rspta);
		}else{
			$t = $venta->listarDetalle($idventa);
			$total=0;
			while ($reg2=$t->fetch_object()) {
				$total=$total+($reg2->precio_venta*$reg2->cantidad-$reg2->descuento);
			}
			$rspta=$venta->mostrar($idventa);
			$rspta['total_venta']=$total;
			$temp = json_encode($rspta);
		
		}
		echo $temp;
		break;

	case 'listarDetalle':
		//recibimos el idventa
		$id=$_GET['id'];
		// $t=$_GET['abono'];

		$rspta=$venta->listarDetalle($id);
		$abono=$venta->mostrar2($id);
		
		$total=0;
		echo ' <thead style="background-color:#A9D0F5">
        <th>Opciones</th>
		<th>Articulo</th>
		<th>Cantidad</th>
        <th>Precio Venta</th>
        <th>Descuento</th>
        <th>Subtotal</th>
       </thead>';
		while ($reg=$rspta->fetch_object()) {
			echo '<tr class="filas">
			<td></td>
			<td>'.$reg->nombre.'</td>
			<td>'.$reg->cantidad.'</td>
			<td>'.$reg->precio_venta.'</td>
			<td>'.$reg->descuento.'</td>
			<td>'.$reg->subtotal.'</td></tr>';
			$total=$total+($reg->precio_venta*$reg->cantidad-$reg->descuento);
		}
		
		if($abono['abono']>$total){
			$abono['abono'] = $total;
		}
		$saldo = $total-$abono['abono'];
		echo '<tfoot>
         <th></th>
         <th></th>
         <th></th>
         <th></th>
		 <th><h4> TOTAL</h4>
		 <h4> IMPORTE</h4>
		 <h4> SALDO</h4></th>
		 <th><h4 id="total">$. '.$total.'</h4><input type="hidden" name="total_venta" id="total_venta" value="'.$total.'">
		 <h4 id="tabono">$. '.$abono['abono'].'</h4><input type="hidden" name="total_abono" id="total_abono" value="'.$abono['abono'].'">
		 <h4 id="saldo">$. '.$saldo.'</h4><input type="hidden" name="saldo" id="saldo"></th>
	   </tfoot>';
		break;

	case 'listarDetalleDev':
		//recibimos el idventa
		$id=$_GET['id'];
		// $t=$_GET['abono'];

		$rspta=$venta->listarDetalleDev($id);
		$abono=$venta->mostrar2($id);
		
		$total=0;
		echo ' <thead style="background-color:#A9D0F5">
		<th>Opciones</th>
		<th>Articulo</th>
		<th>Cantidad</th>
		<th>Precio Venta</th>
		<th>Descuento</th>
		<th>Subtotal</th>
		</thead>';
		while ($reg=$rspta->fetch_object()) {
			if($reg->estado=='X'){
				$t = 'checked></td></tr>';
			}else{
				$t = '> <input type="hidden" name="reingresar[]" id="reingresar[]" value="YES"></td></tr>';
			}
			echo '<tr class="filasdev">
			<td><input type="hidden" name="idarticulodev[]" id="idarticulodev[]" value="'.$reg->idarticulo.'"><input type="hidden" name="iddev[]" id="iddev[]" value="'.$reg->iddetalle_venta.'"></td>
			<td>'.$reg->nombre.'</td>
			<td><input type="hidden" name="cantidaddev[]" id="cantidaddev" value="'.$reg->cantidad.'">'.$reg->cantidad.'</td>
			<td>'.$reg->precio_venta.'</td>
			<td>'.$reg->descuento.'</td>
			
			<td>'.$reg->subtotal.'</td>
			<td><input type="checkbox" name="reingresar[]" id="reingresar[]" "value="on"'.$t;
			$total=$total+($reg->precio_venta*$reg->cantidad-$reg->descuento);
		}
		
		if($abono['abono']>$total){
			$abono['abono'] = $total;
		}
		$saldo = $total-$abono['abono'];
		echo '<tfoot>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
		</tfoot>';
		break;

	case 'imprimir':
		$id   =$idventa;
		$resp = $venta->mostrar2($id);
		//$reg  =$rspta->fetch_object();
		//echo '<script>console.log("'$resp'")</script>';

		if ($resp['tipo_comprobante']=='Ticket') {
			$url='../reportes/exTicket.php?id=';
		}else{
		    $url='../reportes/exFactura.php?id=';
		}
		$url = $url.$id;
		echo $url;
		break;


    case 'listar':
		$rspta=$venta->listar();
		$data=Array();

		while ($reg=$rspta->fetch_object()) {
                 
			$url1='../reportes/exTicket.php?id=';
			
			$url2='../reportes/exFactura.php?id=';
                 
			if($_SESSION['nombre']=='SuperAdmin' OR $_SESSION['facturas']==1){
				if($reg->estado=='Aceptado'){
					$t = 'Aceptado';
					$t2= 'green';
					$t3 = '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idventa.','."1".')"><i class="fa fa-eye"></i></button>'.' '.'<button class="btn btn-danger btn-xs" onclick="anular('.$reg->idventa.')"><i class="fa fa-close"></i></button>';
				}elseif($reg->estado=='Credito'){
					$t = 'Crédito';
					$t2= 'blue';
					$t3 = '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idventa.','."2".')"><i class="fa fa-eye"></i></button>'.' '.'<button class="btn btn-danger btn-xs" onclick="anular('.$reg->idventa.')"><i class="fa fa-close"></i></button>';

				}else{
					$t = 'Anulado';
					$t2= 'red';
					$t3 = '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idventa.','."0".')"><i class="fa fa-eye"></i></button>';
				}
				$data[]=array(
					"0"=>$t3.'<a target="_blank" href="'.$url1.$reg->idventa.'"> <button class="btn btn-primary btn-xs"><i class="fa fa-file"></i></button></a>'.'<a target="_blank" href="'.$url2.$reg->idventa.'"> <button class="btn btn-info btn-xs"><i class="fa fa-file"></i></button></a>',
					"1"=>$reg->fecha,
					"2"=>$reg->cliente,
					"3"=>$reg->usuario,
					"4"=>$reg->tipo_comprobante,
					"5"=>$reg->idventa,
					"6"=>$reg->total_venta,
					"7"=>'<span class="label bg-'.$t2.'">'.$t.'</span>'
				);
			}else{
				$t = $venta->listarDetalle($reg->idventa);
				$total=0;
				while ($reg2=$t->fetch_object()) {
					$total=$total+($reg2->precio_venta*$reg2->cantidad-$reg2->descuento);
				}
				if($reg->abono>$total){
					$reg->abono = $total;
				}
				
				if($total>0){
					if($reg->abono == $total){
						$t = 'Aceptado';
						$t2= 'green';
						$t3 = '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idventa.')"><i class="fa fa-eye"></i></button>'.' '.'<button class="btn btn-danger btn-xs" onclick="anular('.$reg->idventa.')"><i class="fa fa-close"></i></button>';
					}elseif($reg->abono < $total){
						$t = 'Crédito';
						$t2= 'blue';
						$t3 = '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idventa.')"><i class="fa fa-eye"></i></button>'.' '.'<button class="btn btn-danger btn-xs" onclick="anular('.$reg->idventa.')"><i class="fa fa-close"></i></button>';
	
					}elseif($reg->estado=='Anulado'){
						$t = 'Anulado';
						$t2= 'red';
						$t3 = '<button class="btn btn-warning btn-xs" onclick="mostrar('.$reg->idventa.')"><i class="fa fa-eye"></i></button>';
					}
					$data[]=array(
					"0"=>$t3.'<a target="_blank" href="'.$url1.$reg->idventa.'"> <button class="btn btn-primary btn-xs"><i class="fa fa-file"></i></button></a>'.'<a target="_blank" href="'.$url2.$reg->idventa.'"> <button class="btn btn-info btn-xs"><i class="fa fa-file"></i></button></a>',
					"1"=>$reg->fecha,
					"2"=>$reg->cliente,
					"3"=>$reg->usuario,
					"4"=>$reg->tipo_comprobante,
					"5"=>$reg->idventa,
					"6"=>$total,
					"7"=>'<span class="label bg-'.$t2.'">'.$t.'</span>'
				);
					}
			}
			
		}
		$results=array(
             "sEcho"=>1,//info para datatables
             "iTotalRecords"=>count($data),//enviamos el total de registros al datatable
             "iTotalDisplayRecords"=>count($data),//enviamos el total de registros a visualizar
             "aaData"=>$data); 
		echo json_encode($results);
		break;

		case 'selectCliente':
			require_once "../modelos/Persona.php";
			$persona = new Persona();

			$rspta = $persona->listarc();
			echo '<option value="Defecto"> Seleccione un cliente </option>';
			while ($reg = $rspta->fetch_object()) {
				echo '<option value='.$reg->idpersona.'>'.$reg->nombre.'</option>';
			}
			break;
		case 'selectPlacas':
				require_once "../modelos/Venta.php";
				$venta = new Venta();
				$idcliente = isset($_POST["idcliente"])? limpiarCadena($_POST["idcliente"]):"";
				$rspta = $venta->listarPlacas($idcliente);
				echo '<option value="Defecto"> Seleccione una placa </option>';
				while ($reg = $rspta->fetch_object()) {
					echo '<option value='.$reg->idvehiculo.'>'.$reg->placa.'</option>';
				}
		break;

			case 'listarArticulos':
			require_once "../modelos/Articulo.php";
			$articulo=new Articulo();

				$rspta=$articulo->listarActivosVenta();
		$data=Array();

		while ($reg=$rspta->fetch_object()) {
			if($reg->stock<0 AND $reg->tipo=='S'){
				$reg->stock = 1;
			}
			$data[]=array(
            "0"=>'<button class="btn btn-warning" onclick="agregarDetalle('.$reg->idarticulo.',\''.$reg->nombre.'\','.$reg->precio_venta.')"><span class="fa fa-plus"></span></button>',
            "1"=>$reg->nombre,
            "2"=>$reg->categoria,
            "3"=>$reg->codigo,
            "4"=>$reg->stock,
            "5"=>$reg->precio_venta,
			"6"=>"<img src='../files/articulos/".$reg->imagen."' height='50px' width='50px'>",
			"7"=>$reg->tipo
          
              );
		}
		$results=array(
             "sEcho"=>1,//info para datatables
             "iTotalRecords"=>count($data),//enviamos el total de registros al datatable
             "iTotalDisplayRecords"=>count($data),//enviamos el total de registros a visualizar
             "aaData"=>$data); 
		echo json_encode($results);

				break;
		case 'listarArticulosVenta':
			$id=$_GET['id'];
			require_once "../modelos/Articulo.php";
			$articulo=new Articulo();

				$rspta=$articulo->listarActivosVentaDev($id);
			$data=Array();

			while ($reg=$rspta->fetch_object()) {
			if($reg->stock<0 AND $reg->tipo=='S'){
				$reg->stock = 1;
			}
			$data[]=array(
			"0"=>'<button class="btn btn-warning" onclick="agregarDev('.$reg->idarticulo.',\''.$reg->nombre.'\','.$reg->precio_venta.','.$reg->descuento.',\''.$reg->iddetalle_venta.'\','.$reg->stock.')"><span class="fa fa-plus"></span></button>',
			"1"=>$reg->nombre,
			"2"=>$reg->categoria,
			"3"=>$reg->codigo,
			"4"=>$reg->stock,
			"5"=>$reg->precio_venta,
			"6"=>"<img src='../files/articulos/".$reg->imagen."' height='50px' width='50px'>",
			"7"=>$reg->tipo

				);
			}
			$results=array(
				"sEcho"=>1,//info para datatables
				"iTotalRecords"=>count($data),//enviamos el total de registros al datatable
				"iTotalDisplayRecords"=>count($data),//enviamos el total de registros a visualizar
				"aaData"=>$data); 
			echo json_encode($results);

		break;
}
 ?>