<?php 
require_once "../modelos/Consultas.php";
require_once "../modelos/Venta.php";
require_once "../modelos/Ingreso.php";
$consulta = new Consultas();
$ingreso=new Ingreso();
$venta = new Venta();
switch ($_GET["op"]) {
	

    case 'comprasfecha':
    $fecha_inicio=$_REQUEST["fecha_inicio"];
    $fecha_fin=$_REQUEST["fecha_fin"];

		$rspta=$consulta->comprasfecha($fecha_inicio,$fecha_fin);
		$data=Array();
    if($_SESSION['nombre']=="SuperAdmin"){
      while ($reg=$rspta->fetch_object()) {
        $data[]=array(
              "0"=>$reg->fecha,
              "1"=>$reg->usuario,
              "2"=>$reg->proveedor,
              "3"=>$reg->tipo_comprobante,
              "4"=>$reg->idingreso." - ".$reg->serie_comprobante,
              "5"=>$reg->total_compra,
              "6"=>$reg->impuesto,
              "7"=>($reg->estado=='Aceptado')?'<span class="label bg-green">Aceptado</span>':'<span class="label bg-red">Anulado</span>'
                );
      }
    }else{
      
      while ($reg=$rspta->fetch_object()) {
        $t = $ingreso->valorTotal($reg->idingreso);
				$total=0;
				while ($reg2=$t->fetch_object()) {
					$total=$total+($reg2->precio_compra*$reg2->cantidad);
				}
        $data[]=array(
              "0"=>$reg->fecha,
              "1"=>$reg->usuario,
              "2"=>$reg->proveedor,
              "3"=>$reg->tipo_comprobante,
              "4"=>$reg->idingreso." - ".$reg->serie_comprobante,
              "5"=>$total,
              "6"=>$reg->impuesto,
              "7"=>($reg->estado=='Aceptado')?'<span class="label bg-green">Aceptado</span>':'<span class="label bg-red">Anulado</span>'
                );
      }
    }
		$results=array(
             "sEcho"=>1,//info para datatables
             "iTotalRecords"=>count($data),//enviamos el total de registros al datatable
             "iTotalDisplayRecords"=>count($data),//enviamos el total de registros a visualizar
             "aaData"=>$data); 
		echo json_encode($results);
		break;

     case 'ventasfechacliente':
    $fecha_inicio=$_REQUEST["fecha_inicio"];
    $fecha_fin=$_REQUEST["fecha_fin"];
    $idcliente=$_REQUEST["idcliente"];

        $rspta=$consulta->ventasfechacliente($fecha_inicio,$fecha_fin,$idcliente);
        $data=Array();
        if($_SESSION['nombre']=='SuperAdmin' OR $_SESSION['facturas']==1){
          while ($reg=$rspta->fetch_object()) {
              if($reg->estado=='Aceptado'){
                $t = 'Aceptado';
                $t2= 'green';
              }elseif($reg->estado=='Credito'){
                $t = 'Crédito';
                $t2= 'blue';
              }else{
                $t = 'Anulado';
                $t2= 'red';
              }
              $data[]=array(
              "0"=>$reg->fecha,
              "1"=>$reg->usuario,
              "2"=>$reg->cliente,
              "3"=>$reg->tipo_comprobante,
              "4"=>$reg->idventa,
              "5"=>$reg->total_venta,
              "6"=>$reg->impuesto,
              "7"=>'<span class="label bg-'.$t2.'">'.$t.'</span>'
            );
          }
        }else{
          while ($reg=$rspta->fetch_object()) {
              $t = $venta->listarDetalle($reg->idventa);
              $total=0;
              while ($reg2=$t->fetch_object()) {
                $total=$total+($reg2->precio_venta*$reg2->cantidad-$reg2->descuento);
              }
              
              if($reg->abono>$total){
                $reg->abono = $total;
              }
              if($reg->abono == $total){
                $t = 'Aceptado';
                $t2= 'green';
              }elseif($reg->abono < $total){
                $t = 'Crédito';
                $t2= 'blue';
                
              }elseif($reg->estado=='Anulado'){
                $t = 'Anulado';
                $t2= 'red';
                }
              if($total>0){
              $data[]=array(
                "0"=>$reg->fecha,
                "1"=>$reg->usuario,
                "2"=>$reg->cliente,
                "3"=>$reg->tipo_comprobante,
                "4"=>$reg->idventa,
                "5"=>$total,
                "6"=>$reg->impuesto,
                "7"=>'<span class="label bg-'.$t2.'">'.$t.'</span>'
                  );
                }
              }
        }
        $results=array(
             "sEcho"=>1,//info para datatables
             "iTotalRecords"=>count($data),//enviamos el total de registros al datatable
             "iTotalDisplayRecords"=>count($data),//enviamos el total de registros a visualizar
             "aaData"=>$data); 
        echo json_encode($results);
        break;
}
 ?>