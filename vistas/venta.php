<?php
//activamos almacenamiento en el buffer
ob_start();
session_start();
if (!isset($_SESSION['nombre'])) {
  header("Location: login.html");
}else{


require 'header.php';

if ($_SESSION['ventas']==1) {

 ?>
    <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="row">
        <div class="col-md-12">
      <div class="box">
<div class="box-header with-border">
  <h1 class="box-title">Ventas <button class="btn btn-success" onclick="mostrarform(true)"style="background-color:brown;"><i class="fa fa-plus-circle"></i>Agregar</button></h1>
  <div class="box-tools pull-right">
    
  </div>
</div>
<!--box-header-->
<!--centro-->
<div class="panel-body table-responsive" id="listadoregistros">
  <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
    <thead>
      <th>Opciones</th>
      <th>Fecha</th>
      <th>Cliente</th>
      <th>Usuario</th>
      <th>Tipo de comprobante</th>
      <th>Número</th>
      <th>Total Venta</th>
      <th>Estado</th>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
      <th>Opciones</th>
      <th>Fecha</th>
      <th>Cliente</th>
      <th>Usuario</th>
      <th>Tipo de comprobante</th>
      <th>Número</th>
      <th>Total Venta</th>
      <th>Estado</th>
    </tfoot>   
  </table>
</div>
<div class="panel-body" style="height: 400px;" id="formularioregistros">
  <form action="" name="formulario" id="formulario" method="POST">
    <div class="form-group col-lg-8 col-md-8 col-xs-12">
      <label for="">Cliente(*):</label>
      <input class="form-control" type="hidden" name="idventa" id="idventa">
      <select name="idcliente" id="idcliente" class="form-control selectpicker" data-live-search="true" required>
        
      </select>
    </div>
      <div class="form-group col-lg-4 col-md-4 col-xs-12">
      <label for="">Fecha(*): </label>
      <input class="form-control" type="date" name="fecha_hora" id="fecha_hora" required>
    </div>
     <div class="form-group col-lg-5 col-md-6 col-xs-12">
      <label for="">Tipo Comprobante(*): </label>
     <select name="tipo_comprobante" id="tipo_comprobante"  class="form-control selectpicker" required>
     <option value="None" >Seleccione un tipo de comprobante</option>
      <option value="Ticket" >Ticket</option>
      <option value="Factura">Factura</option>
       
     </select>
    </div>
     <div class="form-group col-lg-2 col-md-2 col-xs-6">
      <label for="">Serie: </label>
      <input class="form-control" type="text" name="serie_comprobante" id="serie_comprobante" maxlength="7" placeholder="Serie">
    </div>
    <div class="form-group col-lg-2 col-md-2 col-xs-6">
      <label for="">Impuesto: </label>
      <input class="form-control" type="text" name="impuesto" maxlength="2" id="impuesto">
    </div>
    <div class="form-group col-lg-2 col-md-2 col-xs-3">
      <label for="">Placa(*):</label>
      
      <select name="placa" id="placa" class="form-control selectpicker" data-live-search="true" data-show->
        
      </select>
    </div>
    <div class="form-group col-lg-1 col-md-2 col-xs-3">
      <label for="">Agregar</label>
        <a data-toggle="modal" href="#myModal2">
          <button id="btnAgregarPlaca" type="button" class="btn btn-primary"style="background-color:brown;"><span class="fa fa-plus"></span></button>
        </a>
    </div>
    <div class="form-group col-lg-5 col-md-6 col-xs-12">
    <label for="">Forma de pago(*): </label>
     <select name="tipo_pago" id="tipo_pago"  class="form-control selectpicker" required>
     <option selected value="None" >Seleccione una forma de pago</option>
      <option value="Contado" >Contado</option>
      <option value="Credito">Crédito</option>
       
     </select>
     </div>
    <div class="form-group col-lg-2 col-md-2 col-xs-6">
      <label for="">Agregar importe: </label>
      <input class="form-control" type="text" name="abono" id="abono" maxlength="12" placeholder="Abono">
    </div>
    
    <!-- <label for="">Articulos(*): </label> -->
    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <!-- <label for="">Articulos(*): </label> -->
     <a data-toggle="modal" href="#myModal">
       <button id="btnAgregarArt" type="button" class="btn btn-primary"style="background-color:brown;"><span class="fa fa-plus"></span>Agregar Articulos</button>
     </a>
    </div>
    <div class="form-group col-lg-9 col-lg">
      <label for="">Observaciones: </label>
      <input class="form-control" type="text" name="observaciones" maxlength="256" id="observaciones" placeholder="Observaciones">
    </div>
<div class="form-group col-lg-12 col-md-12 col-xs-12">
     <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
       <thead style="background-color:#A9D0F5">
        <th>Opciones</th>
        <th>Articulo</th>
        <th>Cantidad</th>
        <th>Precio Venta</th>
        <th>Descuento</th>
        <th>Subtotal</th>
       </thead>
       <tfoot>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th>
          <h4> TOTAL</h4>
          <h4> IMPORTE</h4>
          <h4> SALDO</h4></th>
        <th>
          <h4 id="total">$. 0</h4><input type="hidden" name="total_venta" id="total_venta">
          <h4 id="tabono">$. 0</h4><input type="hidden" name="total_abono" id="total_abono">
          <h4 id="saldo">$. 0</h4><input type="hidden" name="total_saldo" id="total_saldo">
        </th>
     </tfoot>
       <tbody>
         
       </tbody>
     </table>
    </div>

    <div class="form-group col-lg-12 col-md-12 col-xs-12">
    <h4   id="dev">Devoluciones</h4>
    <div class="form-group col-lg-1 col-md-2 col-xs-3">
      <label id="dev2" for="">Agregar</label>
        <a data-toggle="modal" href="#myModal3">
          <button id="btnAggDev" type="button" class="btn btn-primary"style="background-color:brown;">Agregar devolución <span class="fa fa-plus"></span></button>
        </a>
    </div>
     <table id="devoluciones" class="table table-striped table-bordered table-condensed table-hover">
       <thead style="background-color:#A9D0F5">
        <th>Opciones</th>
        <th>Articulo</th>
        <th>Cantidad</th>
        <th>Precio Venta</th>
        <th>Descuento</th>
        
        <th>Subtotal</th>
        <th>Reingresar</th>
       </thead>
       <tfoot>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
     </tfoot>
       <tbody>
         
       </tbody>
     </table>
    </div>
    
    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <button class="btn btn-primary" type="submit" id="btnGuardar" style="background-color:brown;"><i class="fa fa-save"></i>  Guardar</button>
      <button class="btn btn-danger" onclick="cancelarform()" type="button" id="btnCancelar" style="background-color:darkslategray;"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
    </div>
  </form>
</div>



<!--fin centro-->
      </div>
      </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>

  <!--Modal-->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 65% !important;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Seleccione un Articulo</h4>
        </div>
        <div class="modal-body">
          <table id="tblarticulos" class="table table-striped table-bordered table-condensed table-hover">
            <thead>
              <th>Opciones</th>
              <th>Nombre</th>
              <th>Categoria</th>
              <th>Código</th>
              <th>Stock</th>
              <th>Precio Venta</th>
              <th>Imagen</th>
              <?php 
                if($_SESSION['nombre']=="SuperAdmin"  OR $_SESSION['tipo_producto']==1){
                  echo "<th>Tipo</th>";
                }
              ?>
            </thead>
            <tbody>
              
            </tbody>
            <tfoot>
              <th>Opciones</th>
              <th>Nombre</th>
              <th>Categoria</th>
              <th>Código</th>
              <th>Stock</th>
              <th>Precio Venta</th>
              <th>Imagen</th>
              <?php 
                if($_SESSION['nombre']=="SuperAdmin" OR $_SESSION['tipo_producto']==1){
                  echo "<th>Tipo</th>";
                }
              ?>
            </tfoot>
          </table>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" type="button" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <!-- fin Modal-->
    <!--Modal-->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModal2Label" aria-hidden="true">
    <div class="modal-dialog" style="width: 65% !important;">
      <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Agregar placa de vehículo:</h4>
        </div>
        <div class="modal-body">
        <div class="form-group col-lg-4 col-md-2 col-xs-6">
          <label for="">Placa(*): </label>
          <input class="form-control" type="text" name="valor_placa" id="valor_placa">
        </div>
        <div class="form-group col-lg-4 col-md-2 col-xs-6">
          <label for="">Cliente(*):</label>
          <select name="idcliente2" id="idcliente2" class="form-control selectpicker" data-live-search="true" required>
            
          </select>
        </div>
        <!-- <label for="">Agregar</label> -->
          <button class="btn btn-warning" onclick="agregarPlaca()"><span class="fa fa-plus"> Agregar </span></button>
            
        </div>
        
        <div class="modal-footer">
          <button class="btn btn-default" type="button" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <!-- fin Modal-->
  <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 65% !important;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Seleccione un Articulo</h4>
        </div>
        <div class="modal-body">
          <table id="tblarticulosventa" class="table table-striped table-bordered table-condensed table-hover">
            <thead>
              <th>Opciones</th>
              <th>Nombre</th>
              <th>Categoria</th>
              <th>Código</th>
              <th>Cantidad Vendida</th>
              <th>Precio Venta</th>
              <th>Imagen</th>
              <?php 
                if($_SESSION['nombre']=="SuperAdmin"  OR $_SESSION['tipo_producto']==1){
                  echo "<th>Tipo</th>";
                }
              ?>
            </thead>
            <tbody>
              
            </tbody>
            <tfoot>
              <th>Opciones</th>
              <th>Nombre</th>
              <th>Categoria</th>
              <th>Código</th>
              <th>Cantidad Vendida</th>
              <th>Precio Venta</th>
              <th>Imagen</th>
              <?php 
                if($_SESSION['nombre']=="SuperAdmin" OR $_SESSION['tipo_producto']==1){
                  echo "<th>Tipo</th>";
                }
              ?>
            </tfoot>
          </table>
        </div>
        <div class="modal-footer">
          <button class="btn btn-default" type="button" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
<?php 
}else{
 require 'noacceso.php'; 
}

require 'footer.php';
 ?>
 <script src="scripts/venta.js"></script>
 <?php 
}

ob_end_flush();
  ?>

