<?php 
//activamos almacenamiento en el buffer
ob_start();
session_start();
if (!isset($_SESSION['nombre'])) {
  header("Location: login.html");
}else{

require 'header.php';
if ($_SESSION['almacen']==1) {
 ?>
    <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="row">
        <div class="col-md-12">
      <div class="box">
<div class="box-header with-border">
  <h1 class="box-title">Artículo <button class="btn btn-success" onclick="mostrarform(true)" id="btnagregar"style="background-color:brown;"><i class="fa fa-plus-circle" ></i>Agregar </button> <a target="_blank"  href="../reportes/rptarticulos.php"><button class="btn btn-info" style="background-color:brown;">Reporte</button></a></h1>
  <div class="box-tools pull-right">
    
  </div>
</div>
<!--box-header-->
<!--centro-->
<div class="panel-body table-responsive" id="listadoregistros">
  <table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
    <thead>
      <th>Opciones</th>
      <th>Nombre</th>
      <th>Categoria</th>
      <th>Codigo</th>
      <th>Stock</th>
      <th>Imagen</th>
      <th>Descripcion</th>
      <th>Estado</th>
      <?php 
        if($_SESSION['nombre']=="SuperAdmin" OR $_SESSION['tipo_producto']==1){
          echo "<th>Tipo</th>";
        }
      ?>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
       <th>Opciones</th>
      <th>Nombre</th>
      <th>Categoria</th>
      <th>Codigo</th>
      <th>Stock</th>
      <th>Imagen</th>
      <th>Descripcion</th>
      <th>Estado</th>
      <?php 
        if($_SESSION['nombre']=="SuperAdmin" OR $_SESSION['tipo_producto']==1){
          echo "<th>Tipo</th>";
        }
      ?>
    </tfoot>   
  </table>
</div>
<div class="panel-body" id="formularioregistros">
  <form action="" name="formulario" id="formulario" method="POST">
    <div class="form-group col-lg-6 col-md-6 col-xs-12">
      <label for="">Nombre(*):</label>
      <input class="form-control" type="hidden" name="idarticulo" id="idarticulo">
      <input class="form-control" type="text" name="nombre" id="nombre" maxlength="100" placeholder="Nombre" required>
    </div>
    <div class="form-group col-lg-6 col-md-6 col-xs-12">
      <label for="">Categoria(*):</label>
      <select name="idcategoria" id="idcategoria" class="form-control selectpicker" data-Live-search="true" required></select>
    </div>
       <div class="form-group col-lg-6 col-md-6 col-xs-12">
      <label for="">Stock</label>
      <input class="form-control" type="number" name="stock" id="stock"  required>
    </div>
       <div class="form-group col-lg-6 col-md-6 col-xs-12">
      <label for="">Descripcion</label>
      <input class="form-control" type="text" name="descripcion" id="descripcion" maxlength="256" placeholder="Descripcion">
    </div>
        <div class="form-group col-lg-6 col-md-6 col-xs-12">
      <label for="">Imagen:</label>
      <input class="form-control" type="file" name="imagen" id="imagen">
      <input type="hidden" name="imagenactual" id="imagenactual">
      <img src="" alt="" width="150px" height="120" id="imagenmuestra">
    </div>
    
    <div class="form-group col-lg-6 col-md-6 col-xs-12">
      <label for="">Codigo:</label>
      <input class="form-control" type="text" name="codigo" id="codigo" placeholder="codigo del prodcuto" required>
      <button class="btn btn-success" type="button" onclick="generarbarcode()"style="background-color:brown;">Generar</button>
      <button class="btn btn-info" type="button" onclick="imprimir()"style="background-color:brown;">Imprimir</button>
      <div id="print">
        <svg id="barcode"></svg>
      </div>
    </div>
    <?php 
                if($_SESSION['nombre']=="SuperAdmin"){
                  echo '<label for="">Tipo de artículo:</label><select name="tipo" id="tipo" class="form-control selectpicker" required>
                  
                  <option value="N">Normal</option>
                  <option value="S">Servicio</option> 
                  <option value="R">Remisión</option>
                  </select>';
                }
      ?>
    <div>
    <p>
    
    

    </p>
    </div>
    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <button class="btn btn-primary" type="submit" id="btnGuardar" style="background-color:brown;"><i class="fa fa-save"></i>  Guardar</button>

      <button class="btn btn-danger" onclick="cancelarform()" type="button" style="background-color:darkslategray;"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
    </div>
  </form>
</div>
<!--fin centro-->
      </div>
      </div>
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<?php 
}else{
 require 'noacceso.php'; 
}
require 'footer.php'
 ?>
 <script src="../public/js/JsBarcode.all.min.js"></script>
 <script src="../public/js/jquery.PrintArea.js"></script>
 <script src="scripts/articulo.js"></script>

 <?php 
}

ob_end_flush();
  ?>