var tabla;

//funcion que se ejecuta al inicio
function init(){
   mostrarform(false,false);
   listar();

   $("#formulario").on("submit",function(e){
   	guardaryeditar(e);
   });

   //cargamos los items al select proveedor
   $.post("../ajax/ingreso.php?op=selectProveedor", function(r){
   	$("#idproveedor").html(r);
   	$('#idproveedor').selectpicker('refresh');
   });

}


//funcion limpiar
function limpiar(){

	//$("#num_comprobante").val("");
	$("#idproveedor").val("");
	$("#proveedor").val("");
	$("#serie_comprobante").val("");
	$("#idingreso").val("");
	//$("#num_comprobante").val("");
	$("#impuesto").val("");
	$("#descuento").val("");
	$("#flete").val("");
	$("#desc").html("$ 0");
	$("#iva").html("$ 0");
	$("#fl").html("$ 0");
	$("#tt").html("$ 0");
	$("#total_compra").val("");
	$(".filas").remove();
	$("#total").html("$ 0");

	//obtenemos la fecha actual
	var now = new Date();
	var day =("0"+now.getDate()).slice(-2);
	var month=("0"+(now.getMonth()+1)).slice(-2);
	var today=now.getFullYear()+"-"+(month)+"-"+(day);
	$("#fecha_hora").val(today);

	//marcamos el primer tipo_documento
	$("#tipo_comprobante").val("Boleta");
	$("#tipo_comprobante").selectpicker('refresh');

}

//funcion mostrar formulario
function mostrarform(flag,flag2){
	limpiar();
	if(flag){
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		if(flag2){
			$("#detalles2").hide();
			$("#detalles").show();
		}else{
			$("#detalles").hide();
			$("#detalles2").show();
		}
		//$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		listarArticulos();

		$("#btnGuardar").hide();
		$("#btnCancelar").show();
		detalles=0;
		$("#btnAgregarArt").show();


	}else{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//cancelar form
function cancelarform(){
	limpiar();
	mostrarform(false,false);
}

//funcion listar
function listar(){
	tabla=$('#tbllistado').dataTable({
		"aProcessing": true,//activamos el procedimiento del datatable
		"aServerSide": true,//paginacion y filrado realizados por el server
		dom: 'Bfrtip',//definimos los elementos del control de la tabla
		buttons: [
                  'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdf'
		],
		"ajax":
		{
			url:'../ajax/ingreso.php?op=listar',
			type: "get",
			dataType : "json",
			error:function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy":true,
		"iDisplayLength":5,//paginacion
		"order":[[0,"desc"]]//ordenar (columna, orden)
	}).DataTable();
}

function listarArticulos(){
	tabla=$('#tblarticulos').dataTable({
		"aProcessing": true,//activamos el procedimiento del datatable
		"aServerSide": true,//paginacion y filrado realizados por el server
		dom: 'Bfrtip',//definimos los elementos del control de la tabla
		buttons: [

		],
		"ajax":
		{
			url:'../ajax/ingreso.php?op=listarArticulos',
			type: "get",
			dataType : "json",
			error:function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy":true,
		"iDisplayLength":5,//paginacion
		"order":[[0,"desc"]]//ordenar (columna, orden)
	}).DataTable();
}
//funcion para guardaryeditar
function guardaryeditar(e){
     e.preventDefault();//no se activara la accion predeterminada 
     //$("#btnGuardar").prop("disabled",true);
     var formData=new FormData($("#formulario")[0]);

     $.ajax({
     	url: "../ajax/ingreso.php?op=guardaryeditar",
     	type: "POST",
     	data: formData,
     	contentType: false,
     	processData: false,

     	success: function(datos){
			mostrarform(false,false);
     		bootbox.alert(datos);
     		//mostrarform(false);
     		listar();
     	}
     });

     limpiar();
}

function mostrar(idingreso){
	$.post("../ajax/ingreso.php?op=mostrar",{idingreso : idingreso},
		function(data,status)
		{
			data=JSON.parse(data);
			mostrarform(true,false);

			$("#idproveedor").val(data.idproveedor);
			$("#idproveedor").selectpicker('refresh');
			$("#tipo_comprobante").val(data.tipo_comprobante);
			$("#tipo_comprobante").selectpicker('refresh');
			$("#serie_comprobante").val(data.serie_comprobante);
			$("#flete").val(data.flete);
			$("#descuento").val(data.descuento);
			//").val(data.num_comprobante);
			$("#fecha_hora").val(data.fecha);
			$("#impuesto").val(data.impuesto);
			$("#idingreso").val(data.idingreso);
			
			//ocultar y mostrar los botones
			$("#btnGuardar").hide();
			$("#btnCancelar").show();
			$("#btnAgregarArt").hide();
		});
	$.post("../ajax/ingreso.php?op=listarDetalle&id="+idingreso,function(r){
		$("#detalles2").html(r);
	});

}


//funcion para desactivar
function anular(idingreso){
	bootbox.confirm("¿Esta seguro de desactivar este dato?", function(result){
		if (result) {
			$.post("../ajax/ingreso.php?op=anular", {idingreso : idingreso}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			});
		}
	})
}

//declaramos variables necesarias para trabajar con las compras y sus detalles
var impuesto=0;
var cont=0;
var detalles=0;

$("#btnGuardar").hide();
$("#tipo_comprobante").change(marcarImpuesto);

function marcarImpuesto(){
	var tipo_comprobante=$("#tipo_comprobante option:selected").text();
	if (tipo_comprobante=='Factura') {
		$("#impuesto").val(impuesto);
	}else{
		$("#impuesto").val("0");
	}
}

function agregarDetalle(idarticulo,articulo,precio_venta,precio_compra_old){
	var cantidad=1;
	var precio_compra=1;
	var precio_total = 0;
	//var precio_venta=1;


	if (idarticulo!="") {
		var subtotal=cantidad*precio_compra;
		precio_total = subtotal;
		var fila='<tr class="filas" id="fila'+cont+'">'+
        '<td><button type="button" class="btn btn-danger" onclick="eliminarDetalle('+cont+')">X</button></td>'+
        '<td><input type="hidden" name="idarticulo[]" value="'+idarticulo+'">'+articulo+'</td>'+
		'<td><input type="number" name="cantidad[]" id="cantidad[]" value="'+cantidad+'"></td>'+
		'<td><input type="hidden" name="precio_compra_old[]" value="'+precio_compra_old+'">'+precio_compra_old+'</td>'+
        '<td><input type="number" name="precio_compra[]" id="precio_compra[]" value="'+precio_compra+'"></td>'+
		'<td><span id="subtotal'+cont+'" name="subtotal">'+subtotal+'</span></td>'+
		'<td><span id="preciototal'+cont+'" name="preciototal">'+precio_total+'</span></td>'+
		'<td><input type="number" name="porcentaje[]" value="'+0+'"></td>'+
		'<td><input type="number" name="precio_venta[]" value="'+precio_venta+'"></td>'+
		
        '<td><button type="button" onclick="modificarSubtotales()" class="btn btn-info"><i class="fa fa-refresh"></i></button></td>'+
		'</tr>';
		cont++;
		detalles++;
		$('#detalles').append(fila);
		modificarSubtotales();

	}else{
		alert("error al ingresar el detalle, revisar los datos del articulo ");
	}
}

function modificarSubtotales(){
	var cant=document.getElementsByName("cantidad[]");
	var porcentajes=document.getElementsByName("porcentaje[]");
	var prec=document.getElementsByName("precio_compra[]");
	var prev=document.getElementsByName("precio_venta[]");
	var prec2=document.getElementsByName("precio_compra_old[]");
	var sub=document.getElementsByName("subtotal");
	var impuesto=document.getElementById("impuesto");
	var descuento=document.getElementById("descuento");
	var flete=document.getElementById("flete");
	

	var y = "0";
	
	for (var i = 0; i < cant.length; i++) {
		
		y = parseInt(cant[i].value,10) + parseInt(y,10);
	}
	if (flete.value!=undefined){
		var x = flete.value/y;
		if (flete_emp.value!=undefined){
			x = (x) + flete_emp.value/y;
		}
	}else{
		var x = 0;
	}
	//var x = flete.value/y;
	// bootbox.alert(x.toString());

	for (var i = 0; i < cant.length; i++) {
		if(prec[i].value==1){
			var inpP=prec2[i];
		}else{
			var inpP=prec[i];
		}
		var inpC=cant[i];

		var inpS=sub[i];
		//alert(flete.value);
		document.getElementsByName("subtotal")[i].innerHTML=inpS.value;
		inpS.value=inpC.value*inpP.value*(1-descuento.value/100);
		inpS.value =Math.round(inpS.value);
		
		
		
		var t = 0;
		t= t + inpS.value*(1+impuesto.value/100)+(x);
		
		t = Math.round(t/cant[i].value);
		document.getElementsByName("preciototal")[i].innerHTML=t;
		prev[i].value=Math.round(t*(1+porcentajes[i].value/100));

	}

	calcularTotales();
}

function calcularTotales(){
	var sub = document.getElementsByName("subtotal");
	var total=0.0;
	var cant=document.getElementsByName("cantidad[]");
	var prec=document.getElementsByName("precio_compra[]");


	var impuesto=document.getElementById("impuesto");
	var descuento=document.getElementById("descuento");
	var flete=document.getElementById("flete");
	var flete_emp=document.getElementById("flete_emp");

	

	for (var i = 0; i < sub.length; i++) {
		//bootbox.alert(cant[i].value.toString()+'-'+sub[i].value.toString())
		total += prec[i].value*cant[i].value;
	}
	//bootbox.alert(total.toString());
	
	 bootbox.alert(flete_emp.value.toString());
	var desc= Math.round(total * descuento.value/100);

	if (flete_emp.value==undefined){
		var iva = Math.round((total-desc) * impuesto.value/100);
	}else{
		
		var iva = Math.round((total-desc + flete_emp.value*1) * impuesto.value/100);
	}


	//var iva = Math.round((total-desc + flete_emp*1) * impuesto.value/100);
	// con descuento
	//var tt  = Math.round(total - desc + iva + flete.value*1);
	// sin descuento
	
	var tt  = Math.round(total + flete_emp.value*1 - desc + iva + flete.value*1 );

	$("#total").html("$" + total);
	$("#desc").html("$" + desc);
	$("#iva").html("$" + iva);
	$("#fl").html("$" + flete.value);
	$("#fl_emp").html("$" + flete_emp.value);
	$("#tt").html("$" + tt);
	$("#total_compra").val(tt);
	evaluar();
}

function evaluar(){

	if (detalles>0) 
	{
		$("#btnGuardar").show();
	}
	else
	{
		$("#btnGuardar").hide();
		cont=0;
	}
}

function eliminarDetalle(indice){
$("#fila"+indice).remove();
calcularTotales();
detalles=detalles-1;

}

init();