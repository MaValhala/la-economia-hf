var tabla;

//funcion que se ejecuta al inicio
function init(){
	mostrarform(false);
	limpiar();
  
   listar();

   $("#formulario").on("submit",function(e){
   	guardaryeditar(e);
   });

   //cargamos los items al select cliente
   $.post("../ajax/venta.php?op=selectCliente", function(r){
   	$("#idcliente").html(r);
   	$('#idcliente').selectpicker('refresh');
   });

}
$('#idcliente').change(function(){
	var idcliente = document.getElementById("idcliente").value;
	if(idcliente==0){

	}else{
		$.post("../ajax/venta.php?op=selectPlacas",{idcliente:idcliente}, function(r){
			// alert(r);
			$("#placa").html(r);
			$('#placa').selectpicker('refresh');
		});
	}
	
});
$('#tipo_pago').change(function(){
	calcularTotales();
	
});


//funcion limpiar
function limpiar(){

	$("#idcliente").val("Defecto");
	$("#idcliente").selectpicker('refresh');
	// $("#cliente").val("");
	$("#serie_comprobante").val("");
	//$("#num_comprobante").val("");
	$("#impuesto").val("");
	$("#tipo_pago").val("None");
	$("#tipo_pago").selectpicker('refresh');
	$("#abono").val(0);
	$("#tabono").val(0);
	$("#total_venta").val("");
	$(".filas").remove();
	$(".filasdev").remove();
	$("#total_venta").val("0");
	$("#observaciones").val("");
	$("#total_abono").val("");
	$("#total_saldo").val("0");
	$("#total").html("0");
	$("#tabono").html("0");
	$("#saldo").html("0");
	$("#idventa").val("");
	$("#valor_placa").val("");
	$("#idcliente2").val("Defecto");
	$("#idcliente2").selectpicker('refresh');
	$("#placa").val("Defecto");
	$("#placa").selectpicker('refresh');

	//obtenemos la fecha actual
	var now = new Date();
	var day =("0"+now.getDate()).slice(-2);
	var month=("0"+(now.getMonth()+1)).slice(-2);
	var today=now.getFullYear()+"-"+(month)+"-"+(day);
	$("#fecha_hora").val(today);

	//marcamos el primer tipo_documento
	$("#tipo_comprobante").val("None");
	$("#tipo_comprobante").selectpicker('refresh');

}

//funcion mostrar formulario
function mostrarform(flag){
	limpiar();
	if(flag){
		$("#listadoregistros").hide();
		$("#formularioregistros").show();
		$("#devoluciones").hide();
		$("#dev").hide();
		$("#dev2").hide();
		$("#btnAggDev").hide();

		//$("#btnGuardar").prop("disabled",false);
		$("#btnagregar").hide();
		listarArticulos();
		$.post("../ajax/venta.php?op=selectCliente", function(r){
			$("#idcliente2").html(r);
			$('#idcliente2').selectpicker('refresh');
		});

		$("#btnGuardar").hide();
		$("#btnCancelar").show();
		detalles=0;
		$("#btnAgregarArt").show();


	}else{
		$("#listadoregistros").show();
		$("#formularioregistros").hide();
		$("#btnagregar").show();
	}
}

//imprimir form
function imprimirform(idventa){
	$.post("../ajax/venta.php?op=imprimir",{idventa : idventa},
		function(data,status)
		{
			window.open(data);
			

			
		});
}

//cancelar form
function cancelarform(){
	limpiar();
	mostrarform(false);
}

// agregar placa
function agregarPlaca(){
	var placa = document.getElementById("valor_placa").value;
	var idcliente = document.getElementById("idcliente2").value;
	// alert(placa);
	if(placa==0){
		alert("Ingrese una placa");
	}else{
		if(idcliente==0){
			alert("Seleccione un cliente");
		}else{
			// alert("Holis");
			$.post("../ajax/venta.php?op=registrarPlaca",{idcliente : idcliente, placa:placa},
			function(data)
			{
				
				// data=JSON.parse(data);
				// mostrarform(false);
				bootbox.alert(data);
				
			});
			$("#valor_placa").val();
			$("#idcliente2").val(0);
			$.post("../ajax/venta.php?op=selectPlacas",{idcliente:idcliente}, function(r){
				$("#placa").html(r);
				$('#placa').selectpicker('refresh');
			});
		}
	}

}

//funcion listar
function listar(){
	tabla=$('#tbllistado').dataTable({
		"aProcessing": true,//activamos el procedimiento del datatable
		"aServerSide": true,//paginacion y filrado realizados por el server
		dom: 'Bfrtip',//definimos los elementos del control de la tabla
		buttons: [
                  'copyHtml5',
                  'excelHtml5',
                  'csvHtml5',
                  'pdf'
		],
		"ajax":
		{
			url:'../ajax/venta.php?op=listar',
			type: "get",
			dataType : "json",
			error:function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy":true,
		"iDisplayLength":5,//paginacion
		"order":[[0,"desc"]]//ordenar (columna, orden)
	}).DataTable();
}

function listarArticulos(){
	tabla=$('#tblarticulos').dataTable({
		"aProcessing": true,//activamos el procedimiento del datatable
		"aServerSide": true,//paginacion y filrado realizados por el server
		dom: 'Bfrtip',//definimos los elementos del control de la tabla
		buttons: [

		],
		"ajax":
		{
			url:'../ajax/venta.php?op=listarArticulos',
			type: "get",
			dataType : "json",
			error:function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy":true,
		"iDisplayLength":5,//paginacion
		"order":[[0,"desc"]]//ordenar (columna, orden)
	}).DataTable();
}
// listar solo los articulos relacionados con la venta
function listarArticulosVenta(idventa){
	tabla=$('#tblarticulosventa').dataTable({
		"aProcessing": true,//activamos el procedimiento del datatable
		"aServerSide": true,//paginacion y filrado realizados por el server
		dom: 'Bfrtip',//definimos los elementos del control de la tabla
		buttons: [

		],
		"ajax":
		{
			url:'../ajax/venta.php?op=listarArticulosVenta&id='+idventa,
			type: "get",
			dataType : "json",
			error:function(e){
				console.log(e.responseText);
			}
		},
		"bDestroy":true,
		"iDisplayLength":5,//paginacion
		"order":[[0,"desc"]]//ordenar (columna, orden)
	}).DataTable();
}
//funcion para guardaryeditar
function guardaryeditar(e){
     e.preventDefault();//no se activara la accion predeterminada 
     //$("#btnGuardar").prop("disabled",true);
     var formData=new FormData($("#formulario")[0]);
	 var idventa = document.getElementById("idventa").value;
	//  alert(obs);
	// alert(document.getElementById("placa").value);
     $.ajax({
     	url: "../ajax/venta.php?op=guardaryeditar&id="+idventa,
     	type: "POST",
     	data: formData,
     	contentType: false,
     	processData: false,

     	success: function(datos){
			if (datos=="No se completo la venta"){
				bootbox.alert(datos);
			}else{
				//console.log(datos)
				// mostrar alerta de estado de la venta
				bootbox.alert(datos);
				// imprimir la venta (en caso de ser exitosa, si no da error xd)
				//imprimirform(datos);
			}
			
			
     		mostrarform(false);
     		listar();
     	}
     });

     limpiar();
}

function mostrar(idventa,estado){
	limpiar();
	var t = 0;
	$("#idventa").val(idventa);
	// alert(document.getElementById("idventa").value);
	$.post("../ajax/venta.php?op=mostrar",{idventa : idventa},
		function(data,status)
		{
			
			// alert(idventa);
			if(estado!=0){
				data=JSON.parse(data);
				mostrarform(true);
				listarArticulosVenta(idventa);
				$("#idventa").val(idventa);
				// alert(document.getElementById("idventa").value);
				
				if(data.abono>data.total_venta){
					data.abono = data.total_venta;
				}
				$("#abono").val(0);
				if(data.abono==data.total_venta){
					$("#tipo_pago").val(data.tipo_pago);
					// $("#btnGuardar").hide();
				}else{
					$("#tipo_pago").val(data.tipo_pago);
					// $("#btnGuardar").show();
				}
				$("#btnGuardar").show();
				$("#tipo_pago").selectpicker('refresh');
				$("#idcliente").val(data.idcliente);
				$("#devoluciones").show(); 
				$("#dev").show();
				$("#dev2").show();
				$("#btnAggDev").show();
				
				$("#observaciones").val(data.observaciones);
				
				$("#total_venta").val(data.total_venta);
				// alert(document.getElementById("total_venta").value);
				$("#idcliente").selectpicker('refresh');
				$("#tipo_comprobante").val(data.tipo_comprobante);
				$("#tipo_comprobante").selectpicker('refresh');
				$("#serie_comprobante").val(data.serie_comprobante);
				//$("#num_comprobante").val(data.num_comprobante);
				$("#fecha_hora").val(data.fecha);
				$("#impuesto").val(data.impuesto);
				// $("#idventa").val(data.idventa);
				$("#total").val("");
				
				//ocultar y mostrar los botones
				// $("#btnGuardar").hide();
				$("#btnCancelar").show();
				$("#btnAgregarArt").hide();
				$.post("../ajax/venta.php?op=selectPlacas",{idcliente:data.idcliente}, function(r){
					// alert(r);
					$("#placa").html(r);
					$("#placa").val(data.idvehiculo);
					$("#placa").selectpicker('refresh');
					// $('#placa').selectpicker('refresh');
				});
			}else{
				data=JSON.parse(data);
				mostrarform(true);
				listarArticulosVenta(idventa);
				$("#idventa").val(idventa);
				// alert(document.getElementById("idventa").value);
				
				if(data.abono>data.total_venta){
					data.abono = data.total_venta;
				}
				$("#abono").val(0);
				if(data.abono==data.total_venta){
					$("#tipo_pago").val(data.tipo_pago);
					// $("#btnGuardar").hide();
				}else{
					$("#tipo_pago").val(data.tipo_pago);
					// $("#btnGuardar").show();
				}
				$("#btnGuardar").show();
				$("#tipo_pago").selectpicker('refresh');
				$("#idcliente").val(data.idcliente);
				$("#devoluciones").show(); 
				$("#dev").show();
				$("#dev2").show();
				$("#btnAggDev").show();
				
				$("#observaciones").val(data.observaciones);
				
				$("#total_venta").val(data.total_venta);
				// alert(document.getElementById("total_venta").value);
				$("#idcliente").selectpicker('refresh');
				$("#tipo_comprobante").val(data.tipo_comprobante);
				$("#tipo_comprobante").selectpicker('refresh');
				$("#serie_comprobante").val(data.serie_comprobante);
				//$("#num_comprobante").val(data.num_comprobante);
				$("#fecha_hora").val(data.fecha);
				$("#impuesto").val(data.impuesto);
				// $("#idventa").val(data.idventa);
				$("#total").val("");
				
				//ocultar y mostrar los botones
				// $("#btnGuardar").hide();
				$("#btnCancelar").show();
				$("#btnAgregarArt").hide();
				// $("#devoluciones").hide();
				// $("#dev").hide();
				// $("#dev2").hide();
				$("#btnAggDev").hide();
				$.post("../ajax/venta.php?op=selectPlacas",{idcliente:data.idcliente}, function(r){
					// alert(r);
					$("#placa").html(r);
					$("#placa").val(data.idvehiculo);
					$("#placa").selectpicker('refresh');
					// $('#placa').selectpicker('refresh');
				});
			}
			
			// alert(data.idvehiculo);
			
			// alert(document.getElementById("idventa").value);
		});
	$.post("../ajax/venta.php?op=listarDetalle&id="+idventa,function(r){
		$("#detalles").html(r);
	});
	$.post("../ajax/venta.php?op=listarDetalleDev&id="+idventa,function(r){
		$("#devoluciones").html(r);
	});
	

}


//funcion para desactivar
function anular(idventa){
	bootbox.confirm("¿Esta seguro de desactivar este dato?", function(result){
		if (result) {
			$.post("../ajax/venta.php?op=anular", {idventa : idventa}, function(e){
				bootbox.alert(e);
				tabla.ajax.reload();
			});
		}
	})
	listar();
}

//declaramos variables necesarias para trabajar con las compras y sus detalles
var impuesto=0;
var cont=0;
var detalles=0;

$("#btnGuardar").hide();
$("#tipo_comprobante").change(marcarImpuesto);
$("#abono").val(0);

function marcarImpuesto(){
	var tipo_comprobante=$("#tipo_comprobante option:selected").text();
	if (tipo_comprobante=='Factura') {
		$("#impuesto").val(impuesto);
	}else{
		$("#impuesto").val("0");
	}
}

function agregarDetalle(idarticulo,articulo,precio_venta){
	var cantidad=1;
	var descuento=0;

	if (idarticulo!="") {
		var subtotal=cantidad*precio_venta;
		var fila='<tr class="filas" id="fila'+cont+'">'+
        '<td><button type="button" class="btn btn-danger" onclick="eliminarDetalle('+cont+')">X</button></td>'+
        '<td><input type="hidden" name="idarticulo[]" value="'+idarticulo+'">'+articulo+'</td>'+
        '<td><input type="number" name="cantidad[]" id="cantidad[]" value="'+cantidad+'"></td>'+
		'<td><input type="number" name="precio_venta[]" id="precio_venta[]" value="'+precio_venta+'"></td>'+
		
		'<td><input type="number" name="descuento[]" value="'+descuento+'"></td>'+
        '<td><span id="subtotal'+cont+'" name="subtotal">'+subtotal+'</span></td>'+
        '<td><button type="button" onclick="modificarSubtotales()" class="btn btn-info"><i class="fa fa-refresh"></i></button></td>'+
		'</tr>';
		cont++;
		detalles++;
		$('#detalles').append(fila);
		modificarSubtotales();

	}else{
		alert("error al ingresar el detalle, revisar las datos del articulo ");
	}
}
// agregar articulo a la devolucion
function agregarDev(idarticulo,articulo,precio_venta,descuento,iddev,cantidad){
	// var cantidad=1;

	if (idarticulo!="") {
		var subtotal=cantidad*precio_venta;
		var fila='<tr class="filasdev" id="filadev'+cont+'">'+
        '<td><button type="button" class="btn btn-danger" onclick="eliminarDev('+cont+')">X</button></td>'+
        '<td><input type="hidden" name="idarticulodev[]" value="'+idarticulo+'">'+articulo+'<input type="hidden" name="iddev[]" value="'+iddev+'"></td>'+
        '<td><input type="number" name="cantidaddev[]" id="cantidaddev[]" value="'+1+'" max="'+cantidad+'"></td>'+
        '<td><span id="precio_venta[]" name="precio_venta[]">'+precio_venta+'</span></td>'+
		'<td><span id="descuento[]" name="descuento[]">'+descuento+'</span></td>'+
		
		
		'<td><span id="subtotal'+cont+'" name="subtotaldev">'+subtotal+'</span></td>'+
		'<td><input type="checkbox" name="reingresar[]" id="reingresar[]"  "value="on"><input type="hidden" name="reingresar[]" id="reingresar[]" value="YES"></td>'+
        '<td><button type="button" onclick="modificarSubtotales()" class="btn btn-info"><i class="fa fa-refresh"></i></button></td>'+
		'</tr>';
		cont++;
		detalles++;
		$('#devoluciones').append(fila);
		// modificarSubtotales();

	}else{
		alert("error al ingresar el detalle, revisar las datos del articulo ");
	}
}

function modificarSubtotales(){
	var cant=document.getElementsByName("cantidad[]");
	var prev=document.getElementsByName("precio_venta[]");
	var desc=document.getElementsByName("descuento[]");
	var sub=document.getElementsByName("subtotal");


	for (var i = 0; i < cant.length; i++) {
		var inpV=cant[i];
		var inpP=prev[i];
		var inpS=sub[i];
		var des=desc[i];


		inpS.value=(inpV.value*inpP.value)-des.value;
		document.getElementsByName("subtotal")[i].innerHTML=inpS.value;
	}

	calcularTotales();
}

function calcularTotales(){
	var sub = document.getElementsByName("subtotal");
	var total=0.0;
	

	for (var i = 0; i < sub.length; i++) {
		total += document.getElementsByName("subtotal")[i].value;
	}
	$("#total").html("$" + total);
	$("#total_venta").val(total);
	if(document.getElementById("tipo_pago").value=='Contado'){
		$("#tabono").html("$" + document.getElementById("total_venta").value);
		$("#total_abono").val(document.getElementById("total_venta").value);
	}else{
		$("#tabono").html("$" + document.getElementById("abono").value);
		$("#total_abono").val(document.getElementById("abono").value);
	}
	var temp = document.getElementById("total_venta").value -  document.getElementById("total_abono").value;
	console.log(temp);
	$("#saldo").html("$" +temp);
	$("#total_saldo").val(total);
	evaluar();
}
function calcularTotales2(){
	var temp = +(document.getElementById("abono").value) + (document.getElementById("total_abono").value);
	console.log(temp);
	bootbox.alert(temp);
	$("#saldo").html("$" +temp);
	$("#total_saldo").val(total);
}


function evaluar(){

	if (detalles>0) 
	{
		$("#btnGuardar").show();
	}
	else
	{
		$("#btnGuardar").hide();
		cont=0;
	}

}

function eliminarDetalle(indice){
$("#fila"+indice).remove();
calcularTotales();
detalles=detalles-1;

}
function eliminarDev(indice){
	$("#filadev"+indice).remove();
	// calcularTotales();
	devoluciones=devoluciones-1;
	
	}

init();