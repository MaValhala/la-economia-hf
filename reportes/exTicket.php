<?php 
//activamos almacenamiento en el buffer
ob_start();
if (strlen(session_id())<1) 
  session_start();

require __DIR__ . '/../autoload.php';
use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

/* Fill in your own connector here */
$connector = new WindowsPrintConnector('POS-80');


if (!isset($_SESSION['nombre'])) {
  echo "debe ingresar al sistema correctamente para vosualizar el reporte";
}else{

if ($_SESSION['ventas']==1) {
# onload="window.print();"
?>


<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="../public/css/ticket.css">
</head>

<body>
	<?php 
// incluimos la clase venta
require_once "../modelos/Venta.php";
$printer = new Printer($connector);
$venta = new Venta();

//en el objeto $rspta obtenemos los valores devueltos del metodo ventacabecera del modelo
$rspta = $venta->ventacabecera2($_GET["id"]);

if ($_SESSION['nombre']=='SuperAdmin' OR $_SESSION['tirillas']==1){
	$reg=$rspta->fetch_object();
}else{
	$reg=$rspta;
}

//establecemos los datos de la empresa
$empresa = "Almacén y taller";
$empresa2 = "LA ECONOMÍA HF";
$documento = "NIT 45621864-5";
$direccion = "Cra 8A # 10A -48 Barrio los Comuneros";
$telefono = "Tel 3174946709";
$email = "correo@mail";
#$logo = EscposImage::load("../files/imgs/logo.png", false);

/* Print top logo */
$printer -> setJustification(Printer::JUSTIFY_CENTER);
#$printer -> graphics($logo);

/* Name of shop */
$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
$printer -> setEmphasis(true);
$printer -> text($empresa. "\n");
$printer -> text($empresa2);
$printer -> setEmphasis(false);
$printer -> feed();
$printer -> selectPrintMode();
$printer -> text($direccion);
$printer -> feed();
$printer -> text($documento);
$printer -> feed();
$printer -> text($telefono);
$printer -> feed();

/* Title of receipt */
$printer -> setEmphasis(true);
$printer -> text("Comprobante de venta\n");
$printer -> setEmphasis(false);
$printer -> text($reg->fecha . "\n");

/* Items */
$printer -> setJustification(Printer::JUSTIFY_LEFT);
$printer -> setEmphasis(true);
$printer -> text( "Cliente: ".$reg->cliente."\n");
$printer -> text($reg->tipo_documento.": ".$reg->num_documento."\n");
$printer -> text( "Comprobante N°: ".$reg->idventa."\n");
$printer -> text( "Forma de pago  : ".$reg->tipo_pago."\n");
$printer -> text( "Placa Vehículo: ".$reg->placa."\n");
$printer -> text( "\n");
$printer -> text( "CANT.   DESCRIPCIÓN                    VALOR    ");
$printer -> text( "================================================");
$printer -> text( "\n");
$printer -> setEmphasis(false);
$rsptad = $venta->ventadetalles($_GET["id"]);
$cantidad=0;
while ($regd = $rsptad->fetch_object()) {
	if(strlen($regd->cantidad)==1){
		$sp1 = "      ";
		$sp_1=" " ;
	}elseif(strlen($regd->cantidad)==2) {
		$sp1 = "     ";
		$sp_1="  ";
	}else{
		$sp1 = "    ";
		$sp_1="   ";
	}
	if(strlen($regd->articulo)<28){
		$sp2 = "";
		for ($i =strlen($regd->articulo) ; $i <= 28; $i++) {
			$sp2 = $sp2." ";
		}
		$printer -> text(" ".$regd->cantidad.$sp1.$regd->articulo.$sp2." ".$regd->subtotal);
		$printer -> feed();
	}else{
		$pt1 = substr($regd->articulo, 0, 25);
		$printer -> text(" ".$regd->cantidad.$sp1.$pt1."     ".$regd->subtotal);
		$printer -> feed();
		
		
		if(strlen($regd->articulo)>50){
			$pt2 =substr($regd->articulo, 26, 50);
			$printer -> text(" ".$sp_1.$sp1.$pt2);
			$printer -> feed();
			if(strlen($regd->articulo)>75){
				$pt3 =substr($regd->articulo, 51, 75);
				$printer -> text(" ".$sp_1.$sp1.$pt3);
				$printer -> feed();
				$pt4 =substr($regd->articulo, 76, 100);
				$printer -> text(" ".$sp_1.$sp1.$pt4);
				$printer -> feed();
				
			}else{
				$pt3 =substr($regd->articulo, 26, strlen($regd->articulo));
				$printer -> text(" ".$sp_1.$sp1.$pt3);
				$printer -> feed();
			}
		}else{
			$pt2 =substr($regd->articulo, 26, strlen($regd->articulo));
			$printer -> text(" ".$sp_1.$sp1.$pt2);
			$printer -> feed();
		}
		
		
	}
	
	$cantidad+=$regd->cantidad;
}
if($reg->abono > $reg->total_venta){
	$reg->abono = $reg->total_venta;
}
$saldo = $reg->total_venta - $reg->abono;
$printer -> text( "================================================");
$printer -> feed();
$printer -> setEmphasis(true);
$printer -> text( "                       TOTAL        ".$reg->total_venta);
$printer -> setEmphasis(false);
$printer -> feed();
$printer -> setEmphasis(true);
$printer -> text( "                       ABONO        ".$reg->abono);
$printer -> setEmphasis(false);
$printer -> feed();
$printer -> setEmphasis(true);
$printer -> text( "                       SALDO        ".$saldo);
$printer -> setEmphasis(false);


/* Footer */
$printer -> feed(2);
$printer -> setJustification(Printer::JUSTIFY_CENTER);
$printer -> text("¡Gracias por su compra!\n");
$printer -> feed(2);


/* Cut the receipt and open the cash drawer */
$printer -> cut();
$printer -> pulse();

$printer -> close();
	 ?>
<div class="zona_impresion">
	<!--codigo imprimir-->
	<br>
	<table border="0" align="center" width="500px">
		<tr>
			<td align="center">
				<!--mostramos los datos de la empresa en el doc HTML-->
				.::<strong> <?php echo $empresa; ?></strong>::.<br>
				.::<strong> <?php echo $empresa2; ?></strong>::.<br>
				<?php echo $documento; ?><br>
				<?php echo $direccion . '-'.$telefono; ?><br>
			</td>
		</tr>
		<tr>
			<td align="center"><?php echo $reg->fecha; ?></td>
		</tr>
		<tr> 
			<td align="center"></td>
		</tr>
		<tr>
			<!--mostramos los datos del cliente -->
			<td>Cliente: <?php echo $reg->cliente; ?>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo $reg->tipo_documento.": ".$reg->num_documento; ?>
			</td>
		</tr>
		<tr>
			<td>
				N° de venta: <?php echo $reg->serie_comprobante; ?>
			</td>
		</tr>
	</table>
	<br>

	<!--mostramos lod detalles de la venta -->

	<table border="0" align="center" width="500px">
		<tr>
			<td>CANT.</td>
			<td>DESCRIPCION</td>
			<td align="right">IMPORTE</td>
		</tr>
		<tr>
			<td colspan="3">=============================================</td>
		</tr>
		<?php
		$rsptad = $venta->ventadetalles($_GET["id"]);
		$cantidad=0;
		while ($regd = $rsptad->fetch_object()) {
		 	echo "<tr>";
		 	echo "<td>".$regd->cantidad."</td>";
		 	echo "<td>".$regd->articulo."</td>";
		 	echo "<td align='right'>$ ".$regd->subtotal."</td>";
		 	echo "</tr>";
		 	$cantidad+=$regd->cantidad;
		 } 

		 ?>
		 <!--mostramos los totales de la venta-->
		 <tr>
			<td colspan="3">=============================================</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td align="right"><b>TOTAL:</b></td>
			<td align="right"><b>$ <?php echo $reg->total_venta; ?></b></td>
		</tr>
		<tr>
			<td colspan="3">N° de articulos: <?php echo $cantidad; ?> </td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" align="center">¡Gracias por su compra!</td>
		</tr>
	</table>
	<br>
</div>
<p>&nbsp;</p>
</body>
</html>



<?php

	}else{
echo "No tiene permiso para visualizar el reporte";
}

}


ob_end_flush();
  ?>