<?php 
//activamos almacenamiento en el buffer
ob_start();
if (strlen(session_id())<1) 
  session_start();

if (!isset($_SESSION['nombre'])) {
  echo "debe ingresar al sistema correctamente para visualizar el reporte";
}else{

if ($_SESSION['ventas']==1) {

//incluimos el archivo factura
require('Factura.php');

//establecemos los datos de la empresa
$empresa = "Almacén y taller LA ECONOMÍA HF";
$documento = "NIT 45621864-5";
$direccion = "Cra 8B # 10A -48 Barrio los cumuneros";
$telefono = "3174446709";
$logo="logo.png";
$ext_logo="png";
$email="equitian33@gmail.com";

//obtenemos los datos de la cabecera de la venta actual
require_once "../modelos/Venta.php";
$venta= new Venta();
$rsptav=$venta->ventacabecera($_GET["id"]);

//recorremos todos los valores que obtengamos
// $regv=$rsptav->fetch_object();
if ($_SESSION['nombre']=='SuperAdmin' OR $_SESSION['facturas']==1){
	$regv=$rsptav->fetch_object();
}else{
	$regv=$rsptav;
}

//configuracion de la factura
$pdf = new PDF_Invoice('p','mm','A4');
$pdf->AddPage();

//enviamos datos de la empresa al metodo addSociete de la clase factura
$pdf->addSociete(utf8_decode($empresa),
                 $documento."\n".
                 utf8_decode("Direccion: "). utf8_decode($direccion)."\n".
                 utf8_decode("Telefono: ").$telefono."\n".
                 "Email: ".$email,$logo,$ext_logo);
// unicamente cuando sea factura legal legal
// $pdf->fact_dev("$regv->tipo_comprobante ","$regv->idventa");

// por ahora es solo un comprobante de venta
$pdf->fact_dev("Comprobante de venta","$regv->idventa");

$pdf->temporaire( "" );
$pdf->addDate($regv->fecha);

if($regv->tipo_pago=='Credito'){
  $regv->tipo_pago = 'Crédito';
}
//enviamos los datos del cliente al metodo addClientAddresse de la clase factura
$pdf->addClientAdresse(utf8_decode($regv->cliente),
                       "Domicilio: ".utf8_decode($regv->direccion), 
                       $regv->tipo_documento.": ".$regv->num_documento, 
                       "Email: ".$regv->email, 
                       "Telefono: ".$regv->telefono,
                       "Forma de pago: ".utf8_decode($regv->tipo_pago),
                       utf8_decode("Placa vehículo: ").$regv->placa
                      );

//establecemos las columnas que va tener lña seccion donde mostramos los detalles de la venta
$cols=array( "CODIGO"=>30,
	         "DESCRIPCION"=>85,
	         "CANT"=>15,
	         "P.U."=>20,
	         "DSCTO"=>18,
	         "SUBTOTAL"=>22);
$pdf->addCols( $cols);
$cols=array( "CODIGO"=>"L",
             "DESCRIPCION"=>"L",
             "CANT"=>"C",
             "P.U."=>"R",
             "DSCTO"=>"R",
             "SUBTOTAL"=>"C" );
$pdf->addLineFormat( $cols);
$pdf->addLineFormat($cols); 

//actualizamos el valor de la coordenada "y" quie sera la ubicacion desde donde empecemos a mostrar los datos 
$y=73;

//obtenemos todos los detalles del a venta actual
$rsptad=$venta->ventadetalles($_GET["id"]);

while($regd=$rsptad->fetch_object()){
  $line = array( "CODIGO"=>"$regd->codigo",
                 "DESCRIPCION"=>utf8_decode("$regd->articulo"),
                 "CANT"=>"$regd->cantidad",
                 "P.U."=>"$regd->precio_venta",
                 "DSCTO"=>"$regd->descuento",
                 "SUBTOTAL"=>"$regd->subtotal");
  $size = $pdf->addLine( $y, $line );
  $y += $size +2;

}  

/*aqui falta codigo de letras*/
require_once "Letras.php";
$V = new EnLetras();

$total=$regv->total_venta; 
$V=new EnLetras(); 
$V->substituir_un_mil_por_mil = true;

 $con_letra=strtoupper($V->ValorEnLetras($total," PESOS")); 
$pdf->addCadreTVAs($con_letra);


//mostramos el impuesto
$pdf->addTVAs( $regv->impuesto, $regv->total_venta, "$ ",$regv->abono);
$pdf->addCadreEurosFrancs("IVA"." $regv->impuesto %");
$pdf->Output('Reporte de Venta' ,'I');

	}else{
echo "No tiene permiso para visualizar el reporte";
}

}

ob_end_flush();
  ?>