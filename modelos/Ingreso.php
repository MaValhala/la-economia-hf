<?php 
//incluir la conexion de base de datos
require "../config/Conexion.php";
if (strlen(session_id())<1) 
	session_start();
class Ingreso{


	//implementamos nuestro constructor
public function __construct(){

}

//metodo insertar registro
public function insertar($idproveedor,$idusuario,$tipo_comprobante,$serie_comprobante,$fecha_hora,$impuesto,$total_compra,$flete,$flete_emp,$descuento,$idarticulo,$cantidad,$precio_compra,$precio_venta,$precio_compra_old){
	if(empty($flete)){
		$flete=0;	
	}
	if(empty($flete_emp)){
		$flete=0;	
	}
	if(empty($descuento)){
		$descuento=0;	
	}
	$sql="INSERT INTO ingreso (idproveedor,idusuario,tipo_comprobante,serie_comprobante,fecha_hora,impuesto,total_compra,flete,flete_empresa,descuento,estado) VALUES ('$idproveedor','$idusuario','$tipo_comprobante','$serie_comprobante','$fecha_hora','$impuesto','$total_compra','$flete','$flete_emp,'$descuento','Aceptado')";
	//return ejecutarConsulta($sql);
	 
	 $idingresonew=ejecutarConsulta_retornarID($sql);
	 $num_elementos=0;
	 $sw=true;
	 if(count($idarticulo)==1){
		if($precio_compra[$num_elementos]==1){
			$precio_compra[$num_elementos]=$precio_compra_old[$num_elementos];
		}
		$sql_detalle="INSERT INTO detalle_ingreso (idingreso,idarticulo,cantidad,precio_compra,precio_venta) VALUES('$idingresonew','$idarticulo[$num_elementos]','$cantidad[$num_elementos]','$precio_compra[$num_elementos]','$precio_venta[$num_elementos]')";
		ejecutarConsulta($sql_detalle) or $sw=false;
	 }else{
		 
		while ($num_elementos < count($idarticulo)) {
			if($precio_compra[$num_elementos]==1){
				$precio_compra[$num_elementos]=$precio_compra_old[$num_elementos];
			}
			$sql_detalle="INSERT INTO detalle_ingreso (idingreso,idarticulo,cantidad,precio_compra,precio_venta) VALUES('$idingresonew','$idarticulo[$num_elementos]','$cantidad[$num_elementos]','$precio_compra[$num_elementos]','$precio_venta[$num_elementos]')";
			ejecutarConsulta($sql_detalle) or $sw=false;
			$num_elementos=$num_elementos+1;
		}
	 }
	 
	 return $sw;
}

public function anular($idingreso){
	
	
	$sw = true;

	$sql="SELECT di.idarticulo,di.cantidad FROM detalle_ingreso di WHERE di.idingreso='$idingreso'" ;
	$res = ejecutarConsulta($sql) or $sw=false;
	
	
	if($sw==true){
		
			while ($reg=$res->fetch_object()) {
				$sql = "UPDATE articulo SET stock=stock-'$reg->cantidad' WHERE idarticulo='$reg->idarticulo'";
				// return $sql;
				ejecutarConsulta($sql) or $sw=false;
			}
		
		
	}else{
		return $sql;
	}
	if($sw==true){
		$sql="UPDATE ingreso SET estado='Anulado' WHERE idingreso='$idingreso'";
		ejecutarConsulta($sql) or $sw=false;
	}else{
		return $sql;
	}
	return $sw;
}


//metodo para mostrar registros
public function mostrar($idingreso){
	$sql="SELECT i.idingreso,DATE(i.fecha_hora) as fecha,i.idproveedor,p.nombre as proveedor,u.idusuario,u.nombre as usuario, i.tipo_comprobante,i.serie_comprobante,i.total_compra,i.impuesto,i.estado,i.flete,i.descuento FROM ingreso i INNER JOIN persona p ON i.idproveedor=p.idpersona INNER JOIN usuario u ON i.idusuario=u.idusuario WHERE idingreso='$idingreso'";
	return ejecutarConsultaSimpleFila($sql);
}


public function listarDetalle($idingreso){

	if ($_SESSION['nombre']=='SuperAdmin'){
		$sql="SELECT di.idingreso,di.idarticulo,a.nombre,a.tipo,di.cantidad,di.precio_compra,di.precio_venta FROM detalle_ingreso di INNER JOIN articulo a ON di.idarticulo=a.idarticulo WHERE di.idingreso='$idingreso'";
	}else{
		$sql="SELECT di.idingreso,di.idarticulo,a.nombre,a.tipo,di.cantidad,di.precio_compra,di.precio_venta FROM detalle_ingreso di INNER JOIN articulo a ON di.idarticulo=a.idarticulo AND a.tipo='N' WHERE di.idingreso='$idingreso'";
	}

	//$sql="SELECT di.idingreso,di.idarticulo,a.nombre,a.tipo,di.cantidad,di.precio_compra,di.precio_venta FROM detalle_ingreso di INNER JOIN articulo a ON di.idarticulo=a.idarticulo WHERE di.idingreso='$idingreso'";
	return ejecutarConsulta($sql);
}

public function valorTotal($idingreso){

	$sql="SELECT di.idingreso,di.idarticulo,a.nombre,a.tipo,di.cantidad,di.precio_compra,di.precio_venta FROM detalle_ingreso di INNER JOIN articulo a ON di.idarticulo=a.idarticulo AND a.tipo='N' WHERE di.idingreso='$idingreso'";
	return ejecutarConsulta($sql);
	
}

//listar registros
public function listar(){
	$sql="SELECT i.idingreso,DATE(i.fecha_hora) as fecha,i.idproveedor,p.nombre as proveedor,u.idusuario,u.nombre as usuario, i.tipo_comprobante,i.serie_comprobante,i.total_compra,i.impuesto,i.estado FROM ingreso i INNER JOIN persona p ON i.idproveedor=p.idpersona INNER JOIN usuario u ON i.idusuario=u.idusuario ORDER BY i.idingreso DESC";
	return ejecutarConsulta($sql);
}

}

 ?>
