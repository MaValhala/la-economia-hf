<?php 
//incluir la conexion de base de datos
require "../config/Conexion.php";
if (strlen(session_id())<1) 
	session_start();
class Venta{


	//implementamos nuestro constructor
public function __construct(){

}

//metodo insertar registro
public function insertar($idcliente,$idusuario,$tipo_comprobante,$serie_comprobante,$fecha_hora,$impuesto,$total_venta,$abono,$idvehiculo,$observaciones,$tipo_pago,$idarticulo,$cantidad,$precio_venta,$descuento){
	if($abono==$total_venta){
		$t = 'Aceptado';
	}else{
		$t = 'Credito';
	}
	if($idvehiculo=="Defecto"){
		$sql="INSERT INTO venta (idcliente,idusuario,tipo_comprobante,serie_comprobante,fecha_hora,impuesto,total_venta,estado,abono,idvehiculo,observaciones,tipo_pago) VALUES ('$idcliente','$idusuario','$tipo_comprobante','$serie_comprobante','$fecha_hora','$impuesto','$total_venta','$t','$abono','1','$observaciones','$tipo_pago')";
	}else{
		$sql="INSERT INTO venta (idcliente,idusuario,tipo_comprobante,serie_comprobante,fecha_hora,impuesto,total_venta,estado,abono,idvehiculo,observaciones,tipo_pago) VALUES ('$idcliente','$idusuario','$tipo_comprobante','$serie_comprobante','$fecha_hora','$impuesto','$total_venta','$t','$abono','$idvehiculo','$observaciones','$tipo_pago')";
	
	}
	// $sql="INSERT INTO venta (idcliente,idusuario,tipo_comprobante,serie_comprobante,fecha_hora,impuesto,total_venta,estado,abono,idvehiculo,observaciones) VALUES ('$idcliente','$idusuario','$tipo_comprobante','$serie_comprobante','$fecha_hora','$impuesto','$total_venta','$t','$abono','$idvehiculo','$observaciones')";
	//return ejecutarConsulta($sql);
	 $idventanew=ejecutarConsulta_retornarID($sql);
	 $num_elementos=0;
	 $sw = $idventanew;
	 while ($num_elementos < count($idarticulo)) {

	 	$sql_detalle="INSERT INTO detalle_venta (idventa,idarticulo,cantidad,precio_venta,descuento,estado) VALUES('$idventanew','$idarticulo[$num_elementos]','$cantidad[$num_elementos]','$precio_venta[$num_elementos]','$descuento[$num_elementos]','F')";

	 	ejecutarConsulta($sql_detalle) or $sw=false;

	 	$num_elementos=$num_elementos+1;
	 }
	 return $sw;
}
public function actualizar($idventa,$idusuario,$tipo_comprobante,$serie_comprobante,$impuesto,$abono,$idvehiculo,$observaciones,$total_venta,$idarticulo,$cantidad,$iddev,$reingreso){
	$sw = true;
	$sql = "SELECT v.abono FROM venta v WHERE v.idventa='$idventa'";
	$rta = ejecutarConsultaSimpleFila($sql);
	$abono = $abono + $rta['abono'];
	if ($_SESSION['nombre']=='SuperAdmin'){
		if($abono >= $total_venta){
			$estado = 'Aceptado';
			if($idvehiculo=="Defecto"){
				$sql="UPDATE venta SET idusuario='$idusuario',tipo_comprobante='$tipo_comprobante',serie_comprobante='$serie_comprobante',impuesto='$impuesto',estado='$estado',abono='$abono',idvehiculo='1',observaciones='$observaciones' WHERE idventa='$idventa'";
				
			}else{
				$sql="UPDATE venta SET idusuario='$idusuario',tipo_comprobante='$tipo_comprobante',serie_comprobante='$serie_comprobante',impuesto='$impuesto',estado='$estado',abono='$abono',idvehiculo='$idvehiculo',observaciones='$observaciones' WHERE idventa='$idventa'";
			}
		}else{
			if($idvehiculo=="Defecto"){
				$sql="UPDATE venta SET idusuario='$idusuario',tipo_comprobante='$tipo_comprobante',serie_comprobante='$serie_comprobante',impuesto='$impuesto',abono='$abono',idvehiculo='1',observaciones='$observaciones' WHERE idventa='$idventa'";
				
			}else{
				$sql="UPDATE venta SET idusuario='$idusuario',tipo_comprobante='$tipo_comprobante',serie_comprobante='$serie_comprobante',impuesto='$impuesto',abono='$abono',idvehiculo='$idvehiculo',observaciones='$observaciones' WHERE idventa='$idventa'";
			}
		}
	}else{
		if($idvehiculo=="Defecto"){
			$sql="UPDATE venta SET idusuario='$idusuario',tipo_comprobante='$tipo_comprobante',serie_comprobante='$serie_comprobante',impuesto='$impuesto',abono='$abono',idvehiculo='1',observaciones='$observaciones' WHERE idventa='$idventa'";
			
		}else{
			$sql="UPDATE venta SET idusuario='$idusuario',tipo_comprobante='$tipo_comprobante',serie_comprobante='$serie_comprobante',impuesto='$impuesto',abono='$abono',idvehiculo='$idvehiculo',observaciones='$observaciones' WHERE idventa='$idventa'";
		}
	}
	
	// $sql="UPDATE venta SET idusuario='$idusuario',tipo_comprobante='$tipo_comprobante',serie_comprobante='$serie_comprobante',impuesto='$impuesto',estado='$estado',abono='$abono',idvehiculo='$idvehiculo',observaciones='$observaciones' WHERE idventa='$idventa'";
	ejecutarConsulta($sql) or $sw=false;
					
	$num_elementos = 0;
	if(count($idarticulo)>0){
		while ($num_elementos < count($idarticulo)) {
			// solo se toman en cuenta los articulos agregados a la devolucion
			$sql = "SELECT dv.cantidad,dv.estado FROM detalle_venta dv WHERE dv.iddetalle_venta='$iddev[$num_elementos]'";
				// se obtiene la cantidad de dicho articulo para revisar si se necesita crear un nuevo registro en
				// el detalle de la venta o solo cambiar el estado a 'D' y posteriormente actualizar el stock de el articulo
				
			$res = ejecutarConsultaSimpleFila($sql) or $sw=false;
			
				if($res['estado']=='F'){
					// $sql = "SELECT dv.cantidad FROM detalle_venta dv WHERE dv.idventa='$idventa' AND dv.idarticulo='$idarticulo[$num_elementos]'";
					// // se obtiene la cantidad de dicho articulo para revisar si se necesita crear un nuevo registro en
					// // el detalle de la venta o solo cambiar el estado a 'D' y posteriormente actualizar el stock de el articulo
					
					// $res = ejecutarConsultaSimpleFila($sql) or $sw=false;
					// return $reg->estado;
					if($sw==true AND $res['cantidad']==$cantidad[$num_elementos]){
						 //para restarle a la factura las devoluciones
						if($reingreso[$num_elementos]!='on'){
						 	$sql = "UPDATE detalle_venta SET estado='D' WHERE iddetalle_venta='$iddev[$num_elementos]'";
						 	ejecutarConsulta($sql) or $sw=false;
						 	if($sw==true ){
						 		$sql = "UPDATE articulo SET stock=stock+'$cantidad[$num_elementos]' WHERE idarticulo='$idarticulo[$num_elementos]'";
								// return $sql; 
								 ejecutarConsulta($sql) or $sw=false;
								 
								if($sw==true){
									$sql = "SELECT dv.precio_venta,dv.descuento FROM detalle_venta dv WHERE iddetalle_venta='$iddev[$num_elementos]'";
									$res2 = ejecutarConsultaSimpleFila($sql) or $sw=false;
									if($sw==true){
										$precio_venta=$res2['precio_venta'];
										$descuento = $res2['descuento'];
										$value = $cantidad[$num_elementos]*$precio_venta;
										if($sw ==true){
											$sql = "UPDATE venta SET total_venta=total_venta-'$value' WHERE idventa='$idventa'";
									
											ejecutarConsulta($sql) or $sw=false;
										}else{
											return $sql;
										}
									}
								}
						 	}else{
						 		return $sql;
						 	}
						 }else{
							$sql = "UPDATE detalle_venta SET estado='X' WHERE iddetalle_venta='$iddev[$num_elementos]'";
							ejecutarConsulta($sql) or $sw=false;
							if($sw==false ){
								return $sql;
							}
						 }
						//return $
						//$sql = "SELECT dv.precio_venta,dv.descuento FROM detalle_venta dv WHERE iddetalle_venta='$iddev[$num_elementos]'";
						//$res2 = ejecutarConsultaSimpleFila($sql) or $sw=false;
						//if($sw==true){
						//	if($sw==true){
						//		$precio_venta=$res2['precio_venta'];
						//		$descuento = $res2['descuento'];
						//		// estado D significa registro creado por una devolucion, no hace parte de la factura
						//		$sql = "INSERT INTO detalle_venta (idventa,idarticulo,cantidad,precio_venta,descuento,estado) VALUES('$idventa','$idarticulo[$num_elementos]','$cantidad[$num_elementos]','$precio_venta','$descuento','D')";
						//		ejecutarConsulta($sql) or $sw=false;
						//		if($sw==true){
						//			// se realiza el reingreso de los productos
						//			$sql = "UPDATE articulo SET stock=stock+'$cantidad[$num_elementos]' WHERE idarticulo='$idarticulo[$num_elementos]'";
						//			ejecutarConsulta($sql) or $sw=false;
						//		}else{
						//			return $sql;
						//		}
						//	}else{
						//		return $sql;
						//	}
						//}else{
						//	return $sql;
						//}
						
						
						
						// return 'golis';
					}elseif($sw==true AND $res['cantidad']>$cantidad[$num_elementos]){
						//para restarle las unidades devueltas a la factura
						
						$sql = "UPDATE detalle_venta SET cantidad=cantidad-'$cantidad[$num_elementos]' WHERE iddetalle_venta='$iddev[$num_elementos]'";
						
						ejecutarConsulta($sql) or $sw=false;
						
						if($sw==true){
							if($reingreso[$num_elementos]!='on'){
								// return $reingreso[$num_elementos];
								$sql = "SELECT dv.precio_venta,dv.descuento FROM detalle_venta dv WHERE iddetalle_venta='$iddev[$num_elementos]'";
								$res2 = ejecutarConsultaSimpleFila($sql) or $sw=false;
								if($sw==true){
									if($sw==true){
										$precio_venta=$res2['precio_venta'];
										$descuento = $res2['descuento'];
										$value = $cantidad[$num_elementos]*$precio_venta;
										
										if($sw ==true){
											$sql = "UPDATE venta SET total_venta=total_venta-'$value' WHERE idventa='$idventa'";
											// return $sql;
											ejecutarConsulta($sql) or $sw=false;
										}else{
											return $sql;
										}
										
										// estado D significa registro creado por una devolucion, no hace parte de la factura
										$sql = "INSERT INTO detalle_venta (idventa,idarticulo,cantidad,precio_venta,descuento,estado) VALUES('$idventa','$idarticulo[$num_elementos]','$cantidad[$num_elementos]','$precio_venta','$descuento','D')";
										ejecutarConsulta($sql) or $sw=false;
										if($sw==true){
											// se realiza el reingreso de los productos
											// $sql = "UPDATE articulo SET stock=stock+'$reg->cantidad' WHERE idarticulo='$reg->idarticulo'";
				
											// ejecutarConsulta($sql) or $sw=false;
											$sql = "UPDATE articulo SET stock=stock+'$cantidad[$num_elementos]' WHERE idarticulo='$idarticulo[$num_elementos]'";
											// return $sql;
											ejecutarConsulta($sql) or $sw=false;
										}else{
											return $sql;
										}
									}else{
										return $sql;
									}
								}else{
									return $sql;
								}
							}else{
								// return $reingreso[$num_elementos];
								$sql = "SELECT dv.precio_venta,dv.descuento FROM detalle_venta dv WHERE iddetalle_venta='$iddev[$num_elementos]'";
								$res2 = ejecutarConsultaSimpleFila($sql) or $sw=false;
								if($sw==true){
									if($sw==true){
										$precio_venta=$res2['precio_venta'];
										$descuento = $res2['descuento'];
										
										if($sw ==true){
											$sql = "UPDATE venta SET total_venta=total_venta-'$value' WHERE idventa='$idventa'";
									
											ejecutarConsulta($sql) or $sw=false;
										}else{
											return $sql;
										}
										
										// estado D significa registro creado por una devolucion, no hace parte de la factura
										$sql = "INSERT INTO detalle_venta (idventa,idarticulo,cantidad,precio_venta,descuento,estado) VALUES('$idventa','$idarticulo[$num_elementos]','$cantidad[$num_elementos]','$precio_venta','$descuento','X')";
										ejecutarConsulta($sql) or $sw=false;
										if($sw==false){
											
											return $sql;
										}
									}else{
										return $sql;
									}
								}else{
									return $sql;
								}
							}
							
						}else{
							return $sql;
						}
						
						
						
					}
					
				
				}
			
			$num_elementos=$num_elementos+1;
		}
		if($sw==true){
			$sw = $idventa;
		}
		return $sw;
	}else{
		return $sw;
	}
	
	
	
	 
	 
	//  return $sql;
}

public function anular($idventa){
	$sw = true;

	$sql="SELECT dv.idarticulo,dv.cantidad FROM detalle_venta dv WHERE dv.idventa='$idventa' AND dv.estado='F'" ;
	$res = ejecutarConsulta($sql) or $sw=false;
	
	
	if($sw==true){
		
			while ($reg=$res->fetch_object()) {
				$sql = "UPDATE articulo SET stock=stock+'$reg->cantidad' WHERE idarticulo='$reg->idarticulo'";
				// return $sql;
				ejecutarConsulta($sql) or $sw=false;
			}
		
		
	}else{
		return $sql;
	}
	if($sw==true){
		$sql="UPDATE venta SET estado='Anulado' WHERE idventa='$idventa'";
		ejecutarConsulta($sql) or $sw=false;
	}else{
		return $sql;
	}
	return $sw;
}


//implementar un metodopara mostrar los datos de unregistro a modificar
public function mostrar($idventa){
	$sql="SELECT v.idventa,DATE(v.fecha_hora) as fecha,v.idcliente,p.nombre as cliente,u.idusuario,u.nombre as usuario, v.tipo_comprobante,v.serie_comprobante,v.total_venta,v.impuesto,v.estado,v.abono,v.idvehiculo,v.observaciones,v.tipo_pago FROM venta v INNER JOIN persona p ON v.idcliente=p.idpersona INNER JOIN usuario u ON v.idusuario=u.idusuario WHERE idventa='$idventa'";
	return ejecutarConsultaSimpleFila($sql);
}
public function mostrar2($idventa){
	$sql="SELECT v.tipo_comprobante,v.abono,v.total_venta FROM venta v WHERE idventa='$idventa'";
	return ejecutarConsultaSimpleFila($sql);
}

public function listarDetalle($idventa){

	if ($_SESSION['nombre']=='SuperAdmin'){
		$sql="SELECT dv.idventa,dv.idarticulo,a.nombre,dv.cantidad,dv.precio_venta,dv.descuento,(dv.cantidad*dv.precio_venta-dv.descuento) as subtotal,v.abono as abono FROM detalle_venta dv INNER JOIN articulo a ON dv.idarticulo=a.idarticulo  INNER JOIN venta v ON v.idventa='$idventa' WHERE dv.idventa='$idventa' AND dv.estado='F'";
	}else{
		$sql="SELECT dv.idventa,dv.idarticulo,a.nombre,dv.cantidad,dv.precio_venta,dv.descuento,(dv.cantidad*dv.precio_venta-dv.descuento) as subtotal,v.abono as abono FROM detalle_venta dv INNER JOIN articulo a ON dv.idarticulo=a.idarticulo AND a.tipo='N' INNER JOIN venta v ON v.idventa='$idventa' WHERE dv.idventa='$idventa' AND dv.estado='F'";
	}
	
	return ejecutarConsulta($sql);
}

// listar detalle devoluciones
public function listarDetalleDev($idventa){

	if ($_SESSION['nombre']=='SuperAdmin'){
		$sql="SELECT dv.iddetalle_venta,dv.idventa,dv.idarticulo,a.nombre,dv.cantidad,dv.precio_venta,dv.descuento,(dv.cantidad*dv.precio_venta-dv.descuento) as subtotal,v.abono, dv.estado FROM detalle_venta dv INNER JOIN articulo a ON dv.idarticulo=a.idarticulo  INNER JOIN venta v ON v.idventa='$idventa' WHERE dv.idventa='$idventa' AND dv.estado!='F'";
	}else{
		$sql="SELECT dv.iddetalle_venta,dv.idventa,dv.idarticulo,a.nombre,dv.cantidad,dv.precio_venta,dv.descuento,(dv.cantidad*dv.precio_venta-dv.descuento) as subtotal,v.abono,dv.estado FROM detalle_venta dv INNER JOIN articulo a ON dv.idarticulo=a.idarticulo AND a.tipo='N' INNER JOIN venta v ON v.idventa='$idventa' WHERE dv.idventa='$idventa' AND dv.estado!='F'";
	}
	
	return ejecutarConsulta($sql);
}

//listar registros

public function valorTotal($idventa){

	$sql="SELECT dv.idventa,dv.idarticulo,a.nombre,dv.cantidad,dv.precio_venta,dv.descuento,(dv.cantidad*dv.precio_venta-dv.descuento) as subtotal FROM detalle_venta dv INNER JOIN articulo a ON dv.idarticulo=a.idarticulo AND a.tipo='N' WHERE dv.idventa='$idventa'";
	return ejecutarConsulta($sql);
	
}

public function agregarPlaca($idcliente,$placa){

	$sql="INSERT INTO vehiculos (idpersona,placa) VALUES ('$idcliente','$placa')";
	return ejecutarConsulta($sql);
	
}
public function listarPlacas($idcliente){

	$sql="SELECT * FROM vehiculos v WHERE v.idpersona ='$idcliente'";
	return ejecutarConsulta($sql);
	
}

public function listar(){
	$sql="SELECT v.idventa,DATE(v.fecha_hora) as fecha,v.idcliente,p.nombre as cliente,u.idusuario,u.nombre as usuario, v.tipo_comprobante,v.serie_comprobante,v.total_venta,v.impuesto,v.estado,v.abono FROM venta v INNER JOIN persona p ON v.idcliente=p.idpersona INNER JOIN usuario u ON v.idusuario=u.idusuario ORDER BY v.idventa DESC";
	return ejecutarConsulta($sql);
}


public function ventacabecera($idventa){
	if ($_SESSION['nombre']=='SuperAdmin' OR $_SESSION['facturas']==1){
		$sql= "SELECT v.idventa,v.tipo_pago, v.abono,vh.placa AS placa, v.idcliente, p.nombre AS cliente, p.direccion, p.tipo_documento, p.num_documento, p.email, p.telefono, v.idusuario, u.nombre AS usuario, v.tipo_comprobante, v.serie_comprobante,  DATE(v.fecha_hora) AS fecha, v.impuesto, v.total_venta FROM venta v INNER JOIN persona p ON v.idcliente=p.idpersona INNER JOIN usuario u ON v.idusuario=u.idusuario INNER JOIN vehiculos vh ON v.idvehiculo = vh.idvehiculo WHERE v.idventa='$idventa'";
		return ejecutarConsulta($sql);
	}else{
		$venta = new Venta();
		$t = $venta->listarDetalle($idventa);
		$total=0;
		while ($reg2=$t->fetch_object()) {
			$total=$total+($reg2->precio_venta*$reg2->cantidad-$reg2->descuento);
		}
		$sql= "SELECT v.idventa,v.tipo_pago, v.abono,vh.placa AS placa, v.idcliente, p.nombre AS cliente, p.direccion, p.tipo_documento, p.num_documento, p.email, p.telefono, v.idusuario, u.nombre AS usuario, v.tipo_comprobante, v.serie_comprobante,  DATE(v.fecha_hora) AS fecha, v.impuesto, v.total_venta FROM venta v INNER JOIN persona p ON v.idcliente=p.idpersona INNER JOIN usuario u ON v.idusuario=u.idusuario INNER JOIN vehiculos vh ON v.idvehiculo = vh.idvehiculo WHERE v.idventa='$idventa'";
		$rta =  ejecutarConsulta($sql);
		$reg=$rta->fetch_object();

		$reg->total_venta = $total;
		
		return $reg;
	}
	
}
public function ventacabecera2($idventa){
	if ($_SESSION['nombre']=='SuperAdmin' OR $_SESSION['tirillas']==1){
		$sql= "SELECT v.idventa,v.tipo_pago, v.abono,vh.placa AS placa, v.idcliente, p.nombre AS cliente, p.direccion, p.tipo_documento, p.num_documento, p.email, p.telefono, v.idusuario, u.nombre AS usuario, v.tipo_comprobante, v.serie_comprobante,  DATE(v.fecha_hora) AS fecha, v.impuesto, v.total_venta FROM venta v INNER JOIN persona p ON v.idcliente=p.idpersona INNER JOIN usuario u ON v.idusuario=u.idusuario INNER JOIN vehiculos vh ON v.idvehiculo = vh.idvehiculo WHERE v.idventa='$idventa'";
		return ejecutarConsulta($sql);
	}else{
		$venta = new Venta();
		$t = $venta->listarDetalle($idventa);
		$total=0;
		while ($reg2=$t->fetch_object()) {
			$total=$total+($reg2->precio_venta*$reg2->cantidad-$reg2->descuento);
		}
		$sql= "SELECT v.idventa,v.tipo_pago, v.abono,vh.placa AS placa, v.idcliente, p.nombre AS cliente, p.direccion, p.tipo_documento, p.num_documento, p.email, p.telefono, v.idusuario, u.nombre AS usuario, v.tipo_comprobante, v.serie_comprobante,  DATE(v.fecha_hora) AS fecha, v.impuesto, v.total_venta FROM venta v INNER JOIN persona p ON v.idcliente=p.idpersona INNER JOIN usuario u ON v.idusuario=u.idusuario INNER JOIN vehiculos vh ON v.idvehiculo = vh.idvehiculo WHERE v.idventa='$idventa'";
		$rta =  ejecutarConsulta($sql);
		$reg=$rta->fetch_object();

		$reg->total_venta = $total;
		
		return $reg;
	}
	
}


public function ventadetalles($idventa){
	if ($_SESSION['nombre']=='SuperAdmin'OR $_SESSION['facturas']==1){
		$sql="SELECT a.nombre AS articulo, a.codigo, d.cantidad, d.precio_venta, d.descuento, (d.cantidad*d.precio_venta-d.descuento) AS subtotal FROM detalle_venta d INNER JOIN articulo a ON d.idarticulo=a.idarticulo WHERE d.idventa='$idventa' AND d.estado='F'";
         return ejecutarConsulta($sql);
	}else{
		$sql="SELECT a.nombre AS articulo, a.codigo, d.cantidad, d.precio_venta, d.descuento, (d.cantidad*d.precio_venta-d.descuento) AS subtotal FROM detalle_venta d INNER JOIN articulo a ON d.idarticulo=a.idarticulo AND a.tipo!='R' WHERE d.idventa='$idventa' AND d.estado='F'";
         return ejecutarConsulta($sql);
	}
	
}


}

 ?>
