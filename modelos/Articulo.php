<?php 
//incluir la conexion de base de datos
require "../config/Conexion.php";
if (strlen(session_id())<1){
	session_start();
}
	
class Articulo{



	//implementamos nuestro constructor
public function __construct(){

}

//metodo insertar regiustro
public function insertar($idcategoria,$codigo,$nombre,$stock,$descripcion,$imagen,$tipo){
	$sql="INSERT INTO articulo (idcategoria,codigo,nombre,stock,descripcion,imagen,condicion,tipo)
	 VALUES ('$idcategoria','$codigo','$nombre','$stock','$descripcion','$imagen','1','$tipo')";
	return ejecutarConsulta($sql);
}

public function editar($idarticulo,$idcategoria,$codigo,$nombre,$stock,$descripcion,$imagen,$tipo){
	if($_SESSION['nombre']=="SuperAdmin"){
		$sql="UPDATE articulo SET idcategoria='$idcategoria',codigo='$codigo', nombre='$nombre',stock='$stock',descripcion='$descripcion',imagen='$imagen',tipo='$tipo'
		WHERE idarticulo='$idarticulo'";
		return ejecutarConsulta($sql);
	}else{
		$sql="UPDATE articulo SET idcategoria='$idcategoria',codigo='$codigo', nombre='$nombre',stock='$stock',descripcion='$descripcion',imagen='$imagen'
		WHERE idarticulo='$idarticulo'";
		return ejecutarConsulta($sql);
	}
}
public function desactivar($idarticulo){
	$sql="UPDATE articulo SET condicion='0' WHERE idarticulo='$idarticulo'";
	return ejecutarConsulta($sql);
}
public function activar($idarticulo){
	$sql="UPDATE articulo SET condicion='1' WHERE idarticulo='$idarticulo'";
	return ejecutarConsulta($sql);
}

//metodo para mostrar registros
public function mostrar($idarticulo){
	$sql="SELECT * FROM articulo WHERE idarticulo='$idarticulo'";
	return ejecutarConsultaSimpleFila($sql);
}

//listar registros 
public function listar(){
	if($_SESSION['nombre']=='SuperAdmin'){
		$sql="SELECT a.idarticulo,a.idcategoria,c.nombre as categoria,a.codigo, a.nombre,a.stock,a.descripcion,a.imagen,a.condicion,a.tipo FROM articulo a INNER JOIN categoria c ON a.idcategoria=c.idcategoria";
		return ejecutarConsulta($sql);
	}else{
		$sql="SELECT a.idarticulo,a.idcategoria,c.nombre as categoria,a.codigo, a.nombre,a.stock,a.descripcion,a.imagen,a.condicion FROM articulo a INNER JOIN categoria c ON a.idcategoria=c.idcategoria WHERE a.tipo='N'";
		return ejecutarConsulta($sql);
	}
}

//listar registros activos
public function listarActivos(){
	$sql="SELECT a.idarticulo,a.tipo,a.idcategoria,c.nombre as categoria,a.codigo, a.nombre,a.stock,a.descripcion,a.imagen,(SELECT precio_venta FROM detalle_ingreso WHERE idarticulo=a.idarticulo ORDER BY iddetalle_ingreso DESC LIMIT 0,1) AS precio_venta,(SELECT precio_compra FROM detalle_ingreso WHERE idarticulo=a.idarticulo ORDER BY iddetalle_ingreso DESC LIMIT 0,1) AS precio_compra,a.condicion FROM articulo a INNER JOIN categoria c ON a.idcategoria=c.idcategoria WHERE a.condicion='1'";
	return ejecutarConsulta($sql);
}

//implementar un metodo para listar los activos, su ultimo precio y el stock(vamos a unir con el ultimo registro de la tabla detalle_ingreso)
public function listarActivosVenta(){
	$sql="SELECT a.idarticulo,a.tipo,a.idcategoria,c.nombre as categoria,a.codigo, a.nombre,a.stock,(SELECT precio_venta FROM detalle_ingreso WHERE idarticulo=a.idarticulo ORDER BY iddetalle_ingreso DESC LIMIT 0,1) AS precio_venta,(SELECT precio_compra FROM detalle_ingreso WHERE idarticulo=a.idarticulo ORDER BY iddetalle_ingreso DESC LIMIT 0,1) AS precio_compra,a.descripcion,a.imagen,a.condicion FROM articulo a INNER JOIN categoria c ON a.idcategoria=c.idcategoria WHERE a.condicion='1' AND a.stock>0  OR a.tipo='S'";
	return ejecutarConsulta($sql);
}
public function listarActivosVentaDev($idventa){
	if($_SESSION['nombre']=='SuperAdmin'){
		$sql="SELECT a.idarticulo,a.tipo,dv.descuento,dv.iddetalle_venta,a.idcategoria,c.nombre as categoria,a.codigo, a.nombre,dv.cantidad AS stock,dv.precio_venta,(SELECT precio_compra FROM detalle_ingreso WHERE idarticulo=a.idarticulo ORDER BY iddetalle_ingreso DESC LIMIT 0,1) AS precio_compra,a.descripcion,a.imagen,a.condicion,dv.estado FROM detalle_venta dv INNER JOIN articulo a ON dv.idarticulo=a.idarticulo INNER JOIN categoria c ON a.idcategoria=c.idcategoria WHERE dv.idventa='$idventa' AND dv.estado='F' AND a.tipo!='S'" ;
	
	}else{
		$sql="SELECT a.idarticulo,a.tipo,dv.descuento,dv.iddetalle_venta,a.idcategoria,c.nombre as categoria,a.codigo, a.nombre,dv.cantidad AS stock,dv.precio_venta,(SELECT precio_compra FROM detalle_ingreso WHERE idarticulo=a.idarticulo ORDER BY iddetalle_ingreso DESC LIMIT 0,1) AS precio_compra,a.descripcion,a.imagen,a.condicion,dv.estado FROM detalle_venta dv INNER JOIN articulo a ON dv.idarticulo=a.idarticulo INNER JOIN categoria c ON a.idcategoria=c.idcategoria WHERE dv.idventa='$idventa' AND dv.estado='F' AND a.tipo='N'" ;
	
	}
	return ejecutarConsulta($sql);
}
}
 ?>
